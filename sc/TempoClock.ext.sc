+ TempoClock {

	ppm {
		^ this.tempo * 60
	}

	ppm_ { |ppm|
		this.tempo_(ppm / 60)
	}

}

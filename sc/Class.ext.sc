+Class {

	*exists { |className|
		^Class.lookup(className).isNil.not
	}

	*lookup { |className|
		^Class.allClasses.detect { |each|
			each.name == className
		}
	}

}

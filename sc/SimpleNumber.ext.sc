+SimpleNumber {

	scaleDegreeInterval { |aNumber|
		var x = aNumber - this;
		^(x >= 0).if {
			x + 1
		} {
			x - 1
		}
	}

	scaleDegreeIntervalParts {
		var lhs = this.rounded.asInteger;
		var rhs = (this.fractionPart.abs * 10).rounded.asInteger;
		^[lhs.sign, lhs.abs, (rhs > 5).if { rhs - 10 } { rhs }]
	}

	expRandRange { |aNumber|
		this.exprand(aNumber)
	}

	mixFill { |aBlock|
		^Mix.fill(this, aBlock)
	}

}

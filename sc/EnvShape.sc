EnvShape {

	var <shape; // Integer
	var <curve; // Float

	init { |aShape aCurve|
		shape = (aShape == 'curve').if {
			5
		} {
			aShape.isSymbol.if {
				Env.shapeNames[aShape]
			} {
				aShape
			}
		};
		curve = aCurve.ifNil {
			0
		}
	}

	shapeAndCurve {
		^[shape, curve]
	}

	//----

	*new { |aShape aCurve|
		^super.new.init(aShape, aCurve)
	}

	*newFromShape { |aShape|
		this.new(aShape, 0)
	}

	*newFromCurve { |aCurve|
		this.new('curve', aCurve)
	}

}

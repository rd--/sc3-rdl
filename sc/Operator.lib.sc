Add {
	*new { |p q|
		^p + q
	}
}

Clip2 {
	*new { |p q|
		^p.clip2(q)
	}
}

Mul {
	*new { |p q|
		^p * q
	}
}

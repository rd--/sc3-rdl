+Object {

	associateWith { |aValue|
		// for blksc3/.js ; c.f. 'x' -> 1; associateWith('x', 1)
		^this -> aValue
	}

}

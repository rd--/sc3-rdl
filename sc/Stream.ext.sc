+ Stream {

	allLengthPrefixedBlocks {
		var blocks = [];
		var next = this.nextLengthPrefixedBlock;
		{ next != nil }.while {
			blocks = blocks.add(next);
			next = this.nextLengthPrefixedBlock
		};
		^blocks
	}

	equalToOnReset { |places|
		var initialAnswer = this.nextN(places);
		this.reset;
		^this.nextN(places) == initialAnswer
	}

	nextLengthPrefixedBlock {
		var count = this.next;
		(count == nil).if {
			^nil
		} {
			^this.nextNOrFewer(count)
		}
	}

	nextNOrFewer { |anInteger|
		var answer = [];
		anInteger.do {
			var next = this.next;
			(next == nil).ifTrue {
				^answer
			};
			answer = answer.add(next)
		};
		^answer
	}

}

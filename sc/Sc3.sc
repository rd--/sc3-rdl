Sc3 {

	//----

	*ccSet { |index value|
		Server.default.sendMsg('/c_set', 11000 + index, value)
	}

	*play { |anObject|
		anObject.play
	}

	*reset {
		CmdPeriod.run
	}

	*swSet { |index, value|
		Server.default.sendMsg('/c_set', 12000 + index, value)
	}

}

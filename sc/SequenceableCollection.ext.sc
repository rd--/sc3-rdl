+SequenceableCollection {

	allTuples {
		var answerSize = this.collect { |item|
			item.size
		}.product;
		^0.to(answerSize - 1).collect { |i|
			var k = i;
			var nextTuple = this.species.newClear(this.size);
			(this.size - 1).to(0, -1).collect { |j|
				var fromArray = this[j];
				nextTuple[j] = fromArray[k % fromArray.size];
				k = k.div(fromArray.size)
			};
			nextTuple
		}
	}

	append { |operand|
		// This is the , operator in Smalltalk and the ++ operator in Supercollider
		^this ++ operand
	}

	asSequenceStream {
		^Pseq(this, 1).asStream
	}

	scaleDegreeIntervals {
		var answer = Array.newClear(this.size - 1);
		(1 .. this.size - 1).do { |index|
			answer[index - 1] = this[index - 1].scaleDegreeInterval(this[index])
		};
		^answer
	}

	deleteTrailingNils {
		var endIndex = this.size - 1;
		{ this[endIndex] == nil }.while {
			endIndex = endIndex - 1;
			(endIndex == 0).if {
				^ this.species.new(0)
			}
		};
		(endIndex == (this.size - 1)).if {
			^this
		} {
			^this.copyRange(0, endIndex)
		}
	}

	extendCyclically { |answerSize|
		// Myself extended to answerSize places by cycling
		this.isEmpty.ifTrue { 'extendCyclically: empty?'.error };
		^0.to(answerSize - 1).collect { |index|
			this.wrapAt(index)
		}
	}

	pchoose { |proportions|
		^this.wchoose(proportions.normalizeSum)
	}

	performWithCrossed { |aSelector aSequence|
		// Written out .x adverb
		var answer = this.species.new(this.size * aSequence.size);
		this.do { |leftItem|
			aSequence.do { |rightItem|
				answer.add(leftItem.perform(aSelector, rightItem))
			}
		};
		^answer
	}

	performWithTable { |aSelector aSequence|
		// Partial form of written out .t adverb.
		var answer = this.species.new(this.size);
		this.do { |leftItem|
			answer.add(leftItem.perform(aSelector, aSequence))
		};
		^answer
	}

	splay {
		^Splay.ar(inArray: this, spread: 1, level: 1, center: 0, levelComp: true)
	}

	splay2 {
		^this.splay
	}

}

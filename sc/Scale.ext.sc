+ Scale {

	*select { |selectFunc|
		^all.copy.putAll(all.parent).select(selectFunc).collect { |scale|
			scale.deepCopy
		}
	}

	*selectAll {
		^this.select { true }
	}

}

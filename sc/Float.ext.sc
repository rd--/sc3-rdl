+Float {

	toArcSine {
		// Lorrain (1980)
		^(pi * this / 2).sin.squared
	}

	toCauchy { |tau|
		// Lorrain (1980)
		^tau * (pi * this).tan
	}

	toExponential { |delta = 1|
		// Lorrain (1980)
		^this.ln.negated / delta
	}

	toHyperbolicCosine {
		// Lorrain (1980)
		^(pi * this / 2).tan.ln
	}

	toLinear { |g = 1|
		// Lorrain (1980)
		^g * (1 - this.sqrt)
	}

	toLogistic { |beta = 0, alpha = 1|
		// Lorrain (1980)
		^(beta.negated - (1 / this - 1).ln) / alpha
	}

}

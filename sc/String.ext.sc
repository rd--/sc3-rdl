+String {

	asFloatOnError { |onError|
		this.isFloatString.if {
			^this.asFloat
		} {
			^onError.value
		}
	}

	isFloatString {
		^"^[+-]?([0-9]+([.][0-9]*)?([eE][+-]?[0-9]+)?|[.][0-9]+([eE][+-]?[0-9]+)?)\$".matchRegexp(this)
	}

}

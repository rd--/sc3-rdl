+Object {

	assertChannelCountIsANumber {
		this.isNumber.not.if {
			Error("assertChannelCountIsANumber: numChannels not a number?").throw
		}
	}

}

+In {

	init { arg numChannels ... argBus;
		inputs = argBus.asArray;
		^this.initOutputs(numChannels.assertChannelCountIsANumber, rate)
	}

}

+InFeedback {

	init { arg numChannels ... argBus;
		^this.initOutputs(numChannels.assertChannelCountIsANumber, rate)
	}

}

+ Integer {

	to { |end, step = 1|
		// Replace Interval definition with Array definition
		^this.series(this + step, end)
	}

}

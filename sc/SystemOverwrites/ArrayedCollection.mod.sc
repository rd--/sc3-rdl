+ ArrayedCollection {

	add { |item|
		// (this.size == this.maxSize).if { "%.add: see ArrayedCollection.basicAdd or List.add".format(this.class.name).postln };
		^this.basicAdd(item)
	}

	asArray {
		// ^this
		^Array.newFrom(this)
	}

}

+Interval {

	asUGenInput {
		^this.asArray.asUGenInput
	}

	mixFill { |aBlock|
		^this.asArray.collect(aBlock).sum
	}

}

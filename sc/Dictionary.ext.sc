+Dictionary {

	/* merge dictionaries with left bias */
	mergeLeft { |right|
		var dict = Dictionary(this.size + right.size);
		right.keysValuesDo { |key value|
			dict[key.asString] = value
		};
		this.keysValuesDo { |key value|
			dict[key.asString] = value
		};
		^dict
	}

}

R {

	//----

	*drawScsyndefFile { |fileName|
		("hsc3-dot scsyndef-draw" + fileName).unixCmd
	}

	*midiConnectAll {
		MIDIIn.connectAll
	}

	*resampleRms { |anArray, numFrames = 512|
		var k = (anArray.size / numFrames).floor;
		var rmsFunction = { |z|
			var r = 0;
			k.do { |i|
				r = r + anArray[z + i].squared
			};
			(r / k).sqrt
		};
		^(0 .. numFrames - 1).collect { |i|
			rmsFunction.value(i * k)
		}
	}

	*ugenCategoryDict { |selFunc|
		^Dictionary.with(*R.ugenCategoryTable(selFunc));
	}

	*ctl { |name="Anonymous" default=0 minval=0 maxval=1 warp='lin' lag=0.0|
		var genName = { |i|
			name ++ ":" ++ (i + 1).asString
		};
		var genCtl = { |i j|
			NamedControl.new(i, j, spec: ControlSpec.new(minval, maxval, warp, lag, j))
		};
		(default.size == 0).if {
			^genCtl.value(name,default)
		} {
			^default.collect { |i j|
				genCtl.value(genName.value(j),i)
			}
		}
	}

	*cmdPeriodRetainServerState {
		var freeServers = CmdPeriod.freeServers;
		CmdPeriod.freeServers = false;
		CmdPeriod.run;
		CmdPeriod.freeServers = freeServers;
	}

}

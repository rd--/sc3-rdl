+Number {

	ampComp { |freq root exp|
		^((root ? 60.midicps) / freq) ** (exp ? (1/3))
	}

	cpsPch {
		^this.cpsMidi.midiPch
	}

	doubleSoftClipper { |upperLim = 1, lowerLim = -1, slope = 1, xOffFactor = 1, upperSkew = 1, lowerSkew = 1|
		// https://ccrma.stanford.edu/~jatin/ComplexNonlinearities/DoubleSoftClipper.html
		var x = this;
		var xOff = (1 / slope) * (slope ** xOffFactor);
		(x > 0).if {
			x = (x - xOff) * upperSkew;
			(x >= (1 / slope)).if {
				^upperLim
			} {
				(x <= (-1 / slope)).if {
					^ 0
				} {
					^ (3 / 2) * upperLim * ((slope * x) - ((slope * x) ** 3 / 3)) / 2 + (upperLim / 2)
				}
			}
		} {
			x = (x + xOff) * lowerSkew;
			(x >= (1 / slope)).if {
				^ 0
			} {
				(x <= (-1 / slope)).if {
					^ lowerLim
				} {
					^ (3 / 2) * lowerLim.negated * ((slope * x) - ((slope * x) ** 3 / 3)) / 2 + (lowerLim / 2)
				}
			}
		}
	}

	midiNoteNumberToOctavePitchClass {
		var octave = (this / 12).floor - 1;
		var zero = (octave + 1) * 12;
		var pitchClass = (this - zero) / 100;
		^octave + pitchClass
	}

	midiPch {
		^this.midiNoteNumberToOctavePitchClass
	}

	octavePitchClassToMidiNoteNumber {
		var octave = this.floor;
		var pitchClass = (this - octave) * 100;
		^(octave + 1) * 12 + pitchClass
	}

	pchCps {
		^this.pchMidi.midiCps
	}

	pchMidi {
		^this.octavePitchClassToMidiNoteNumber
	}

}

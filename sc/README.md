# File names

The file name conventions are:

- _name_.sc - implement class _name_
- _name_.ext.sc - extend class _name_ (define additional instance and class methods)
- _name_.lib.sc - implement a set of classes forming library _name_ (ordinarily many small classes)
- _name_.mod.sc - modify class _name_ (redefine existing instance and class methods)
- _name_.msg.sc - implement message _name_ at a set of existing classes (define a protocol or extend a protocol to other classes)

Sc requires all modifications to be in a directory called _SystemOverwrites_.

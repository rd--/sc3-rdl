Uid {

	var <id;

	init {
		id = UniqueID.next
	}

	*new {
		^super.new.init
	}

}

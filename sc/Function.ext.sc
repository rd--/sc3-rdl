+Function {

	plotCenteredDetail { |numberOfChannels startTime duration|
		this.plotDetail(numberOfChannels, startTime - (duration / 2), duration)
	}

	plotDetail { |numberOfChannels startTime duration|
		var runTime = startTime + duration + 1E-4;
		this.getToFloatArray(
			runTime,
			action: { |interleavedData|
				{
					var secondsToSamples = { |seconds|
						(seconds * Server.default.sampleRate * numberOfChannels).asInteger
					};
					var startIndex = secondsToSamples.value(startTime);
					var endIndex = startIndex + secondsToSamples.value(duration);
					var ofInterest = interleavedData.copyRange(startIndex, endIndex);
					var channelData = ofInterest.unlace(numberOfChannels);
					channelData.plot
				}.defer
			}
		)
	}

}

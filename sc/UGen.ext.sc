// Math
+ UGen {

	add { |operand|
		^this + operand
	}

	bitShiftRight { |operand|
		^this.rightShift(operand)
	}

	expRange { |from to|
		^this.linExpFrom(from, to: to)
	}

	fdiv { |operand|
		^this / operand
	}

	idiv { |operand|
		^this.div(operand)
	}

	if { |trueBlock falseBlock|
		var trueUgen = trueBlock.value;
		var falseUgen = falseBlock.value;
		^(this * (trueUgen - falseUgen)) + falseUgen;
	}

	linExpFrom { |from to|
		^LinExp.multiNew(this.rate, this, -1, 1, from, to)
	}

	linLinFrom { |from to|
		^LinLin.multiNew(this.rate, this, -1, 1, from, to)
	}

	midiCps {
		^this.midicps
	}

	midiRatio {
		^this.midiratio
	}

	mix {
		^Mix(this)
	}

	mul { |operand|
		^this * operand
	}

	mulAdd { |mul add|
		^this * mul + add
	}

	negated {
		^this.neg
	}

	roundTo { |operand|
		^this.round(operand)
	}

	rounded {
		^this.round(1)
	}

	sub { |operand|
		^this - operand
	}

	truncateTo { |operand|
		^this.trunc(operand)
	}

}

// Smalltalk
+UGen {
	transpose {
		// approx.
		^this.flop
	}
}

// Ugen
+UGen {

	clearBuf { ClearBuf(this);
		^this
	}

	mrg { |rhs|
		^this
	}
}

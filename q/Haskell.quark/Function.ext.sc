+ Function {

	iterate { |anObject anIntegerOrNil|
		anIntegerOrNil.isNil.if {
			^this.iterateL(anObject)
		} {
			^this.iterateN(anObject, anIntegerOrNil)
		}
	}

	iterateL { |anObject|
		var state = anObject;
		^FuncStream {
			var next = state;
			state = this.value(state);
			next
		} {
			state = anObject
		}
	}

	iterateN { |anObject anInteger|
		anInteger.do {
			anObject = this.value(anObject)
		};
		^anObject
	}

	map { |aCollection|
		^aCollection.collect(this)
	}

}

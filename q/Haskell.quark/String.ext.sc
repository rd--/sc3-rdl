+String {

	lines {
		^this.split($\n)
	}

	words {
		^this.split($ )
	}

}

+SequenceableCollection {

	dropWhile { |aBlock|
		this.size.do { |index|
			aBlock.value(this[index]).not.if {
				^this.copyRange(index, this.size - 1)
			}
		};
		^this.species.new(0)
	}

	sortOn { |keyFunc compareFunc|
		// [t] -> (t -> u) -> (u -> u -> Bool) -> [t]
		^this[this.collect(keyFunc).order(compareFunc)]
	}

	takeUntil { |anObject|
		var endIndex = this.indexOf(anObject);
		(endIndex == nil).if {
			^this
		} {
			^this.copyRange(0, endIndex - 1)
		}
	}

	takeUntilNil {
		^this.takeUntil(nil)
	}

	takeWhile { |aBlock|
		this.size.do { |index|
			aBlock.value(this[index]).not.if {
				(index < 1).if {
					^this.species.new(0)
				} {
					^this.copyRange(0, index - 1)
				}
			}
		};
		^this
	}

	unlines {
		^this.join($\n)
	}

	unwords {
		^this.join($ )
	}

}

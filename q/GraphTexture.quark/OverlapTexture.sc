OverlapTexture {

	*ar { |graphFunc, sustainTime = 4.0, transitionTime = 4.0, overlap = 2|
		^Mix.fill(
			n: overlap,
			function: { |index|
				var trg = Impulse.kr(freq: 1 / (sustainTime + (transitionTime * 2)), phase: index / overlap);
				var snd = graphFunc.value(trg);
				var env = Env.new(levels: [0, 1, 1, 0], times: [transitionTime, sustainTime, transitionTime], curve: \sin);
				snd * EnvGen.kr(envelope: env, gate: trg, levelScale: 1, levelBias: 0, timeScale: 1, doneAction: 0)
			}
		)
	}

	*new { |graphFunc, sustainTime = 4.0, transitionTime = 4.0, overlap = 2|
		^OverlapTexture.ar(graphFunc, sustainTime, transitionTime, overlap)
	}

}

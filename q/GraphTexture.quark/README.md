# GraphTexture

Implements _OverlapTexture_ and _XFadeTexture_ graph constructors, similar to those in Sc2.

See also: _ScheduledTexture.quark_

See
[OverlapTexture.help.scd](https://gitlab.com/rd--/sc3-rdl/-/blob/master/q/GraphTexture.quark/OverlapTexture.help.scd)
[XFadeTexture.help.scd](https://gitlab.com/rd--/sc3-rdl/-/blob/master/q/GraphTexture.quark/XFadeTexture.help.scd)

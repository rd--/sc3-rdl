/*

XFade is not just Overlap of two.
The XFade envelope is a little more complicated than the Overlap envelope.

The envelope shape is as below (monospace font required!)

|       |       |
/---\___/---\___/
____/---\___/---\

*/
XFadeTexture {

	*ar { |graphFunc sustainTime=4.0 transitionTime=4.0|
		var envDur = (sustainTime + transitionTime) * 2;
		var voiceFunc = { |phase|
			var trg = Impulse.kr(freq: 1 / envDur, phase: phase);
			var snd = graphFunc.value(trg);
			var env = Env(
				levels: [0, 1, 1, 0, 0],
				times: [transitionTime, sustainTime, transitionTime, sustainTime],
				curve: 'sin'
			);
			snd * EnvGen.kr(
				envelope: env,
				gate: trg,
				levelScale: 1,
				levelBias: 0,
				timeScale: 1,
				doneAction: 0
			)
		};
		^voiceFunc.value(0) + voiceFunc.value(0.5)
	}

	*new { |graphFunc sustainTime=4.0 transitionTime=4.0|
		^XFadeTexture.ar(graphFunc, sustainTime, transitionTime)
	}

}

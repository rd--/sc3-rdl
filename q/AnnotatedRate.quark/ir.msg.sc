+ExpRand {
	*ir { |lo hi|
		^this.new(lo, hi)
	}
}

+IRand {
	*ir { |lo hi|
		^this.new(lo, hi)
	}
}

+LinRand {
	*ir { |lo hi minmax|
		^this.new(lo, hi, minmax)
	}
}

+LocalBuf {
	*ir { |numFrames numChannels|
		^this.new(numFrames, numChannels)
	}
}

+NRand {
	*ir { |lo hi n|
		^this.new(lo, hi, n)
	}
}

+Rand {
	*ir { |lo hi|
		^this.new(lo, hi)
	}
}

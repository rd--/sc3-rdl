+FFT {
	*kr { |buffer in hop wintype active winsize|
		^this.new(buffer, in, hop, wintype, active, winsize)
	}
}

+PV_Diffuser {
	*kr { |buffer trig=0.0|
		^this.new(buffer, trig)
	}
}

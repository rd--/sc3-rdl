# AnnotatedRate

Implements _ir_ methods for _Rand_ &etc.

	SinOsc.ar(Rand.ir(333, 555), 0) * 0.1

Implements _dr_ methods for _Dseq_ &etc.

	var trig = Impulse.kr(7, 0);
	var freq = Demand.kr(trig, 0, Dseq.dr([333, 555, 777], inf));
	SinOsc.ar(freq.lag, 0) * 0.1

See also: _DefaultRate_, _FilterMethods_, _RewriteRate_

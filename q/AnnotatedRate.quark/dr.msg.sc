+Dbrown {
	*dr { |lo hi step length|
		^this.new(lo, hi, step, length)
	}
}

+Dbufrd {
	*dr { |bufnum phase loop|
		^this.new(bufnum, phase, loop)
	}
}

+Dbufwr {
	*dr { |input bufnum phase loop|
		^this.new(input, bufnum, phase, loop)
	}
}

+Dseries {
	*dr { |start step length|
		^this.new(start, step, length)
	}
}

+Dswitch {
	*dr { |list index|
		^this.new(list, index)
	}
}

+Dwhite {
	*dr { |lo hi length|
		^this.new(lo, hi, length)
	}
}

+ListDUGen {
	*dr { |list repeats=1|
		^this.new(list, repeats)
	}
}

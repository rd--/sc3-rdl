SphericalCoordinate : Number { var <>rho, <>theta, <>phi;

	*new { arg rho, theta, phi;
		^super.newCopyArgs(rho, theta, phi)
	}

	== { |anObject|
		^anObject.isSphericalCoordinate and:
		{ rho == anObject.rho and:
			{ theta == anObject.theta and:
				{ phi == anObject.phi
				}
			}
		}
	}

	asSphericalCoordinate {
		^this
	}

	asCartesianCoordinate {
		^CartesianCoordinate(this.x, this.y, this.z)
	}

	isSphericalCoordinate {
		^true
	}

	printOn { |stream|
		stream << this.class.name << "( " << rho << ", " << theta << ", " << phi << " )";
	}

	x {
		^rho * sin(theta) * cos(phi)
	}

	y {
		^rho * sin(theta) * sin(phi)
	}

	z {
		^rho * cos(theta)
	}

}

+Object {

	isSphericalCoordinate {
		^false
	}

}

CartesianCoordinate : Number { var <>x, <>y, <>z;

	*new { |x y z|
		^super.newCopyArgs(x, y, z)
	}

	== { |anObject|
		^anObject.isCartesianCoordinate and:
		{ x == anObject.x and:
			{ y == anObject.y and:
				{ z == anObject.z }
			}
		}
	}

	asArray {
		^[x, y, z]
	}

	asSphericalCoordinate {
		^SphericalCoordinate(
			(x.squared + y.squared + z.squared).sqrt,
			(x.squared + y.squared).sqrt.atan2(z),
			y.atan2(x)
		)
	}

	isCartesianCoordinate {
		^true
	}

	phi {
		^atan2(y, x)
	}

	printOn { |stream|
		stream << this.class.name << "( " << x << ", " << y << ", " << z << " )";
	}

	rho {
		^(x.squared + y.squared + z.squared).sqrt
	}

	theta {
		^atan2(z, (x.squared + y.squared).sqrt)
	}

}

+Object {

	isCartesianCoordinate {
		^false
	}

}

EvalListener  {

	//--------

	classvar listener;

	*init {
		listener.free;
		listener = OSCFunc(
			path: '/eval',
			func: { |message time_ address receivePort_|
				var result = message[1].asString.interpret;
				address.sendMsg("/result", result.asString)
			}
		).permanent_(true)
	}

}

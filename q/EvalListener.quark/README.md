# EvalListener

EvalListener installs a permanent _OSCFunc_ to listen for _/eval_ Osc messages.

The _/eval_ message has one argument, a string, which is interpreted.

The result is converted to a string and sent as a _/result_ reply message.

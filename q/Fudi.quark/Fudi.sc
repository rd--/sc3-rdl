+String {

	parseFudi {
		var removeEmpty = { |x|
			x.reject { |str|
				str.size == 0
			}
		};
		var msg = removeEmpty.value(this.split($;));
		var elem = msg.collect { |m|
			removeEmpty.value(m.split($ ))
		};
		var parseElem = { |str|
			str[0].isDecDigit.if {
				str.indexOf($.).isNil.if {
					str.asInteger
				} {
					str.asFloat
				}
			} {
				(str[0] == $").if {
					str.copyRange(1,str.size - 2)
				} {
					str.asSymbol
				}
			}
		};
		^elem.collect { |x|
			x.collect(parseElem)
		}
	}

}

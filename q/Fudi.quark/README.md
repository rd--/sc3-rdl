# Fudi

Simple parser for a subset of Fudi generating [[Symbol|String|Int|Float]].

In particular the string should not be newline terminated and symbols/strings should not include escaped white space.

	"/a 1 c; /d 2.5 f;".parseFudi == [['/a',1,'c'],['/d',2.5,'f']]

# DefaultRate.quark

Implements _new_ methods for Ugens that select default rates.

Oscillators Ugens will default to the highest applicable rate.

	SinOsc([333, 555], 0) * 0.1

Filter Ugens will default to the rate of their _signal_ input, as unary and binary operators do in the ordinary case.

	Resonz(PinkNoise(), [333, 555], 1) * 0.1

To modify the rate of a Ugen sub-graph see _RewriteRate.quark_.

* * *

Select is handled especially, i.e.

	var freq = Select(SinOsc(1, 0).range(0, 3), [333, 555, 777]);
	SinOsc(freq.lag, 0) * 0.1

Splay considers the rate of the first input:

	Splay({ SinOsc(Rand(333, 777), 0) } ! 9) * 0.1

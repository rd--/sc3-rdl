+Select {
	*new { arg which, array;
		var defaultRate = which.rate;
		(defaultRate == 'audio').if {
			array.every { |each|
				each.rate == 'audio'
			}.not.if {
				defaultRate = 'control'
			}
		};
		^this.multiNewList([defaultRate, which] ++ array)
	}
}

+Splay {
	*new { arg inArray, spread = 1, level = 1, center = 0.0, levelComp = true;
		var defaultRate = inArray[0].rate.rateToSelector;
		^Splay.performList(defaultRate, [inArray, spread, level, center, levelComp])
	}
}

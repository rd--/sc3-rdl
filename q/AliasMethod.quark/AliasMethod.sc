AliasMethod {

	var <alias, <probability;

	next {
		var column = rrand(0, this.probability.size - 1);
		var coinToss = rrand(0.0, 1.0) < this.probability[column];
		coinToss.if {
			^column
		} {
			^this.alias[column]
		}
	}

	*new { |w|
		var probabilities = w.copy;
		var count = probabilities.size;
		var probability = Array.newClear(count);
		var alias = Array.newClear(count);
		var average = 1.0 / count;
		var small = List();
		var large = List();
		count.do { |i|
			(probabilities[i] >= average).if {
				large.add(i)
			} {
				small.add(i)
			}
		};
		{
			(small.isEmpty || large.isEmpty).not
		}.while {
			var less = small.pop;
			var more = large.pop;
			probability[less] = probabilities[less] * count;
			alias[less] = more;
			probabilities[more] = probabilities[more] + probabilities[less] - average;
			(probabilities[more] >= (1 / count)).if {
				large.add(more)
			} {
				small.add(more)
			}
		};
		{ small.isEmpty.not }.while {
			probability[small.pop] = 1
		};
		{ large.isEmpty.not }.while {
			probability[large.pop] = 1
		};
		^super.newCopyArgs(alias, probability)
	}

}

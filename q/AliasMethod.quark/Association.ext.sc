+Association {

	choose { |shape|
		^this.randomChoice(shape)
	}

	chooseStream {
		^this.randomChoiceStream
	}

	randomChoice { |shape|
		var r = AliasMethod(this.key.normalizeSum);
		var e = this.value;
		shape.isNil.if {
			^e[r.next]
		} {
			^{ e[r.next] } ! shape
		}
	}

	randomChoiceStream {
		var r = AliasMethod(this.key.normalizeSum);
		var e = this.value;
		^FuncStream { e[r.next] } { }
	}

}

# AliasMethod

The Walker/Vose "Alias Method" algorithm for sampling from a discrete probability distribution,
c.f. <https://en.wikipedia.org/wiki/Alias_method>.

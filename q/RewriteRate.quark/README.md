# RewriteRate.quark

Implement a _kr_ instance method for Ugens that rewrites nodes from _audio_ rate to _control_ rate.

This is typically used in conjuction with _DefaultRate.quark_.

Compare the graphs:

	SinOsc(SinOsc(3, 0) * 333 + 555, 0) * 0.1

and

	SinOsc(SinOsc(3, 0).kr * 333 + 555, 0) * 0.1

See [RewriteRate.help.scd](https://gitlab.com/rd--/sc3-rdl/-/blob/master/q/RewriteRate.quark/RewriteRate.help.scd)

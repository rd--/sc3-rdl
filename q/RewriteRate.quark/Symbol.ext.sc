+Symbol {

	rateToSelector {
		^this.switch(
			'audio', { 'ar' },
			'control', { 'kr' },
			'scalar', { 'ir' },
			'demand', { 'dr' },
			{ "Symbol>>rateToSelector".error }
		)
	}

}

# FilterMethods

Implements instance methods for filter Ugens, such as _Resonz_ at Ugen and Array.

Filter Ugens default to the rate of their _signal_ input.

	PinkNoise.ar().resonz([333, 555], 1) * 0.1

See also: _DefaultRate.quark_ and _RewriteRate.quark_.

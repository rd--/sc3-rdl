+UGen {

	/* ---- */

	*categoryAssocArray { |selFunc|
		^UGen.categorySet(selFunc).asArray.sort.collect { |cat|
			cat -> UGen.inCategory(selFunc, cat)
		}
	}

	*categoryTable { |selFunc|
		^UGen.subclasses.select(selFunc).collect { |ugen|
			ugen.name -> ugen.categoriesOf
		}
	}

	*categorySet { |selFunc|
		^UGen.categoryTable(selFunc).collect('value').flatten.asSet
	}

	*inCategory { |selFunc cat|
		var inCat = { |assoc|
			assoc.value.includesEqual(cat)
		};
		^UGen.categoryTable(selFunc).select(inCat).collect('key').sort
	}

}

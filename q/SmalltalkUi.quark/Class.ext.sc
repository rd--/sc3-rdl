+Class {

	categoriesOf {
		^this.scDocField { |doc| doc.categories } { ["Undocumented"] }
	}

	isCore {
		^this.scDocField { |doc| doc.isExtension.not } { true }
	}

	isExtension {
		^this.scDocField { |doc| doc.isExtension } { false }
	}

	methodDefParam { |methodName|
		var m = this.class.findRespondingMethodFor(methodName);
		m.notNil.if {
			var k = m.argNames;
			var v = m.prototypeFrame;
			var s = CollStream.new(collection: String.new(maxSize: 256));
			s << this.name << '.' << methodName << '(';
				(1 .. k.size - 1).do { |i|
					(i > 1).if {
						s << ','
					};
					s << k[i] << ': ' << v[i]
				};
			s << ')';
			^s.collection
		};
		^nil
	}

	summaryOfAndInstanceCreation { |methodNames|
		var stream = CollStream.new(collection: String.new(maxSize: 512));
		var hasParam = false;
		var genExemplarFor = { |methodName|
			var defParam = this.methodDefParam(methodName);
			defParam.notNil.if {
				hasParam = true;
				stream << defParam << nl
			}
		};
		stream << this.summaryOf << nl << nl;
		methodNames.do(genExemplarFor);
		hasParam.not.if {
			genExemplarFor.value('new')
		};
		^stream.collection
	}

	summaryOf {
		^this.scDocField { |doc| doc.summary } { "Undocumented" }
	}

	scDoc {
		^SCDoc.documents["Classes/" ++ this.name]
	}

	scDocField { |ifPresent ifAbsent|
		var doc = this.scDoc;
		doc.isNil.if {
			^ifAbsent.value
		} {
			^ifPresent.value(doc)
		}
	}

	subclassTreeFromWithIndent { |indent|
		var subclasses = this.subclasses;
		(indent ++ this.name).postln;
		subclasses.ifNotNil {
			subclasses.copy.sortWith('name', '<=').do { |class|
				class.subclassTreeFromWithIndent(indent ++ "  ")
			}
		}
	}

	subclassTreeText {
		this.subclassTreeFromWithIndent("")
	}

}

Ui {

	//----

	*setDefaultFont {
		Font.setDefault(Font.new(name: "DejaVu Sans", size: 11, bold: false, italic: false))
	}

	*viewImageFile { |fileName|
		var ext = PathName.new(path: fileName).extension;
		var i = (ext == "svg").if {
			Image.openSVG(path: fileName)
		} {
			Image.open(path: fileName)
		};
		var w = Window.new(name: i.name, bounds: i.bounds, resizable: false, border: true, scroll: false);
		w.onClose_ {
			i.free
		};
		w.view.setBackgroundImage(image: i, tileMode: 1, alpha: 1, fromRect: nil);
		w.view.fixedSize_(Size.new(width: i.width, height: i.height));
		w.front
	}

	*chooseDirectory { |initialPath recvFunc|
		FileDialog.new(
			okFunc: { |x|
				recvFunc.value(x[0])
			},
			cancelFunc: {
				recvFunc.value(nil)
			},
			fileMode: 2,
			acceptMode: 0,
			stripResult: false,
			path: initialPath
		)
	}

	*chooseFile { |initialPath recvFunc|
		FileDialog.new(
			okFunc: { |x|
				recvFunc.value(x[0])
			},
			cancelFunc: {
				recvFunc.value(nil)
			},
			fileMode: 1,
			acceptMode: 0,
			stripResult: false,
			path: initialPath
		)
	}

	*saveFile { |initialPath recvFunc|
		FileDialog.new(
			okFunc: { |x|
				recvFunc.value(x[0])
			},
			cancelFunc: {
				recvFunc.value(nil)
			},
			fileMode: 0,
			acceptMode: 1,
			stripResult: false,
			path: initialPath
		)
	}

	*isEnterOrReturnKey { |n|
		^((n == 0x01000004) || (n == 0x01000005))
	}

	*enterKeyAction { |view recvFunc|
		var f = { |doc char mod unicode keycode key|
			Ui.isEnterOrReturnKey(key).if {
				recvFunc.value(view)
			}
		};
		view.keyDownAction_(f)
	}

	*button { |parent text recvFunc|
		var b = Button.new(parent: parent)
			.states_([[text,Color.black,Color.white]])
			.action_(recvFunc);
			Ui.enterKeyAction(b,recvFunc);
			^b;
		}

		*chooseFromButtons { |items recvFunc|
			var r = Rect.new(left: 120, top: 60, width: 355, height: 55);
			var w = Window.new(name: "Choose From", bounds: r, resizable: false, border: true, scroll: false);
			var s = items.collect { |each| each.asString };
			var z = nil;
			var b = s.collect { |e ix|
				Ui.button(w, e) {
					z = ix;
					w.close
				}
			};
			w.layout_(HLayout.new(*b));
			w.onClose_ {
				recvFunc.value(z)
			};
			w.front
		}

		*chooseFromListWithInformation { |items recvFunc infoFunc|
			var r = Rect.new(left: 120, top: 60, width: 700, height: 394);
			var w = Window.new(name: "Choose From", bounds: r, resizable: false, border: true, scroll: false);
			var s = items.collect { |each| each.asString };
			var t = TextField.new(parent: w);
			var l = ListView.new(parent: w).items_(s);
			var i = TextView.new(parent: w);
			var doAccept = {
				recvFunc.value(s.indexOf(l.items[l.value]));
				w.close
			};
			var bAccept = Ui.button(w, "Accept", doAccept);
			var bCancel = Ui.button(w, "Cancel") {
				recvFunc.value(nil); w.close
			};
			var inTxt = { |txt x|
				txt.find(x, ignoreCase: true).notNil
			};
			var doFilter = { |x|
				var narrow = {
					s.select { |txt|
						inTxt.value(txt, x.value)
					}
				};
				var sSel = x.value.isEmpty.if(s, narrow);
				l.items_(sSel)
			};
			t.keyUpAction_(doFilter);
			l.selectionAction_ {
				i.string_(infoFunc.value(s.indexOf(l.items[l.value])))
			};
			l.enterKeyAction_(doAccept); // this should also run on double-click
			w.layout_(VLayout.new(t,l,i,HLayout.new(bAccept,bCancel,nil)));
			w.front
		}

		*chooseItemFromListWithInformation { |items recvFunc infoFunc|
			var f = { |ix|
				ix.notNil.if {
					recvFunc.value(items[ix])
				}
			};
			Ui.chooseFromListWithInformation(items: items, recvFunc: f, infoFunc: infoFunc);
		}

		*chooseFromList { |items recvFunc|
			var r = Rect.new(left: 120, top: 60, width: 200, height: 355);
			var w = Window.new(name: "Choose From", bounds: r, resizable: false, border: true, scroll: false);
			var s = items.collect { |each| each.asString };
			var t = TextField.new(parent: w);
			var l = ListView.new(parent: w).items_(s);
			var z = nil;
			var doAccept = {
				z = s.indexOf(l.items[l.value]);
				w.close
			};
			var bAccept = Ui.button(w, "Accept", doAccept);
			var bCancel = Ui.button(w, "Cancel") {
				w.close
			};
			var inTxt = { |txt x|
				txt.find(x,ignoreCase:true).notNil
			};
			var doFilter = { |x|
				var narrow = {
					s.select { |txt|
						inTxt.value(txt, x.value)
					}
				};
				var sSel = x.value.isEmpty.if(s, narrow);
				l.items_(sSel)
			};
			t.keyUpAction_(doFilter);
			l.enterKeyAction_(doAccept); // this should also run on double-click
			w.layout_(VLayout.new(t,l,HLayout.new(bAccept,bCancel)));
			w.onClose_ {
				recvFunc.value(z)
			};
			w.front;
		}

		*chooseFrom { |items recvFunc|
			(items.size <= 6).if {
				Ui.chooseFromButtons(items, recvFunc)
			} {
				Ui.chooseFromList(items, recvFunc)
			}
		}

		*chooseItemFrom { |items recvFunc|
			var f = { |ix|
				ix.isNil.if {
					recvFunc.value(nil)
				} {
					recvFunc.value(items[ix])
				}
			};
			Ui.chooseFrom(items: items, recvFunc: f)
		}

		*chooseFontName { |recvFunc|
			Ui.chooseItemFrom(items: Font.availableFonts.sort, recvFunc: recvFunc)
		}

		*chooseFont { |recvFunc|
			var r = Rect.new(left: 150, top: Window.screenBounds.height - 500, width: 500, height: 224);
			var w = Window.new(name: "Choose Font", bounds: r, resizable: false, border: true, scroll: false);
			var f = PopUpMenu.new(parent: w).items_(Font.availableFonts);
			var n = PopUpMenu.new(parent: w).items_([6,7,8,9,10,11,12,14,18,24,36,48,60,72,96].collect { |each | each.asString });
			var b = CheckBox.new(parent: w,text: "Bold");
			var i = CheckBox.new(parent: w,text: "Italic");
			var t = TextView.new(parent: w).string_("Sphinx of black quartz, judge my vow.\n\n0 1 2 3 4 5 6 7 8 9");
			var z = nil;
			var doGetFont = {
				Font.new(name: f.item, size: n.item.asInteger, bold: b.value, italic: i.value)
			};
			var doAccept = {
				z = doGetFont.value;
				w.close
			};
			var bAccept = Ui.button(w, "Accept", doAccept);
			var bCancel = Ui.button(w, "Cancel") {
				z = nil;
				w.close
			};
			var doEdit = {
				t.font_(doGetFont.value).refresh
			};
			f.action_(doEdit);
			n.action_(doEdit);
			b.action_(doEdit);
			i.action_(doEdit);
			f.valueAction_(2);
			n.valueAction_(6);
			w.layout_(VLayout.new(HLayout.new(f,n,b,i,nil),t,HLayout.new(bAccept,bCancel,nil)));
			w.onClose_ {
				recvFunc.value(z)
			};
			w.front;
		}

		*chooseDefaultFont {
			Ui.chooseFont { |x|
				x.notNil.if {
					Font.setDefault(x)
				}
			}
		}

		*chooseSubclassOf { |class recvFunc|
			var z = class.allSubclasses.sortOn('name', '<=');
			Ui.chooseItemFrom(items: z, recvFunc: recvFunc)
		}

		*chooseUgen { |selFunc recvFunc|
			var z = UGen.allSubclasses.select(selFunc).sortOn('name', '<=');
			Ui.chooseItemFrom(z, recvFunc: recvFunc)
		}

		*chooseUgenWithInformation { |selFunc recvFunc|
			var z = UGen.allSubclasses.select(selFunc).sortOn('name', '<=');
			var f = { |ix|
				z[ix].summaryOfAndInstanceCreation(['ar','kr','ir'])
			};
			Ui.chooseItemFromListWithInformation(z, recvFunc: recvFunc, infoFunc: f)
		}

		*confirm { |text recvFunc|
			var r = Rect.new(left: 120, top: 60, width: 300, height: 68);
			var w = Window.new(name: "Confirm", bounds: r, resizable: false, border: true, scroll: false);
			var z = nil;
			var t = StaticText(w).string_(text);
			var m = [
				MenuAction("Yes (y)") {
					z = true;
					w.close
				}.shortcut_("Ctrl+Y"),
				MenuAction("No (n)") {
					z = false;
					w.close
				}.shortcut_("Ctrl+N"),
				MenuAction("Cancel (c)") {
					w.close
				}.shortcut_("Ctrl+C")
			];
			w.layout_(VLayout.new(t,ToolBar.new(*m)));
			w.onClose_ {
				recvFunc.value(z)
			};
			w.front
		}

		*inform { |text|
			var r = Rect.new(left: 120, top: 60, width: 300, height: 68);
			var w = Window.new(name: "Inform", bounds: r, resizable: false, border: true, scroll: false);
			var t = StaticText(w).string_(text);
			var bOk = Ui.button(w, "Ok") {
				w.close
			};
			w.layout_(VLayout.new(t,HLayout.new(bOk,nil)));
			w.front
		}

		*request { |prompt recvFunc|
			var r = Rect.new(left: 120, top: 60, width: 200, height: 90);
			var w = Window.new(name: "Request", bounds: r, resizable: false, border: true, scroll: false);
			var p = StaticText(w).string_(prompt);
			var t = TextField.new(parent: w, bounds: Rect(0,0,200,270));
			var z = nil;
			var doAccept = {
				z = t.string;
				w.close
			};
			var bAccept = Ui.button(w, "Accept", doAccept);
			var bCancel = Ui.button(w, "Cancel") {
				z = nil;
				w.close
			};
			t.focus(true);
			t.action_(doAccept);
			w.layout_(VLayout.new(p,t,HLayout.new(bAccept,bCancel,nil)));
			w.onClose_ {
				recvFunc.value(z)
			};
			w.front;
		}

		*editText { |text recvFunc menuActions|
			var r = Rect.new(left: 120, top: 120, width: 600, height: 270);
			var w = Window.new(name: "Edit Text", bounds: r, resizable: false, border: true, scroll: false);
			var t = TextView.new(parent: w).string_(text).focus(true);
			var z = nil;
			var bAccept = Ui.button(w, "Accept") {
				z = t.string;
				w.close
			};
			var bCancel = Ui.button(w, "Cancel") {
				z = nil;
				w.close
			};
			menuActions.notNil.if {
				var asMenuAction = { |assoc|
					MenuAction(assoc.key, assoc.value)
				};
				t.setContextMenuActions(*menuActions.value(t).collect(asMenuAction))
			};
			w.layout_(VLayout.new(t,HLayout.new(bAccept,bCancel,nil)));
			w.onClose_ {
				recvFunc.value(z)
			};
			w.front
		}

		// there should be a chooseFromAssocArrayTree that returns indices, however...
		*chooseItemFromAssocArrayTree { |assocArray recvFunc|
			var r = Rect.new(left: 120, top: 60, width: 300, height: 530);
			var w = Window.new(name: "Choose From Tree",bounds: r, resizable: false, border: true, scroll: false);
			var t = TreeView.new(parent: w);
			var z = nil;
			var doAccept = {
				z = t.currentItem.strings[0];
				z.postln;
				w.close
			};
			var bAccept = Ui.button(w, "Accept", doAccept);
			var bCancel = Ui.button(w, "Cancel") {
				z = nil;
				w.close
			};
			t.columns_([""]); // no column header
			t.action_(doAccept);
			assocArray.collect { |x i|
				t.insertItem(i,[x.key]);
				x.value.collect { |y j|
					t.itemAt(i).insertChild(j,[y])
				}
			};
			w.layout_(VLayout.new(t,HLayout.new(bAccept,bCancel,nil)));
			w.onClose_ {
				recvFunc.value(z)
			}; /* if(z.isNil,{nil},{[z.parent.index,z.index]}) */
			w.front;
		}

		*chooseFromAssocArrayMenu { |assocArray recvFunc|
			var f = { |x i|
				Menu(*x.value.collect { |y j|
					MenuAction(y) {
						recvFunc.value(i,j)
					}
				}).title_(x.key)
			};
			var m = Menu(*assocArray.collect(f));
			m.front
		}

		*chooseItemFromAssocArrayMenu { |assocArray recvFunc|
			Ui.chooseFromAssocArrayMenu(assocArray) { |i j|
				recvFunc.value(assocArray[i].value[j])
			}
		}

		*chooseUgenFromTree { |selFunc recvFunc|
			var f = { |x|
				recvFunc.value(x.asString.interpret)
			};
			^Ui.chooseItemFromAssocArrayTree(assocArray: UGen.categoryAssocArray(selFunc: selFunc), recvFunc: f)
		}

		*chooseUgenFromMenu { |selFunc recvFunc|
			var f = { |x|
				recvFunc.value(x.asString.interpret)
			};
			^Ui.chooseItemFromAssocArrayMenu(assocArray: UGen.categoryAssocArray(selFunc: selFunc), recvFunc: f)
		}

		*commandList { |items, recvFunc, onSelection = false|
			var r = Rect.new(left: 120, top: 60, width: 300, height: 532);
			var w = Window.new(name: "Choose From", bounds: r, resizable: false, border: true, scroll: false);
			var s = items.collect('asString');
			var t = TextField.new(parent: w);
			var l = ListView.new(parent: w).items_(s);
			var z = nil;
			var doAccept = {
				z = s.indexOf(l.items[l.value]);
				recvFunc.value(z)
			};
			var inTxt = { |txt x|
				txt.find(x,ignoreCase:true).notNil
			};
			var doFilter = { |x|
				var narrow = {
					s.select { |txt|
						inTxt.value(txt, x.value)
					}
				};
				var sSel = x.value.isEmpty.if(s, narrow);
				l.items_(sSel)
			};
			t.keyUpAction_(doFilter);
			l.enterKeyAction_(doAccept); // this should also run on double-click
			onSelection.if {
				l.selectionAction_(doAccept)
			};
			w.layout_(VLayout.new(t,l));
			w.front
		}

		*scsynthCommandList { |items onSelection server|
			var recvFunc = { |i|
				server.sendMsg(*items[i])
			};
			Ui.commandList(items: items, recvFunc: recvFunc, onSelection: onSelection)
		}

		*scsynthCommandListFudi { |fudiPacket onSelection server|
			Ui.scsynthCommandList(items: fudiPacket.parseFudi, onSelection: onSelection, server: server)
		}

		*playFunctionWithNdefUi { |aBlock|
			var aSymbol = ("_gensym_" ++ Date.getDate.stamp).asSymbol;
			var anNdef = Ndef.new(aSymbol, aBlock).mold(2).play;
			/* add metadata specs - https://github.com/supercollider/supercollider/issues/5515 */
			anNdef.specs.keysValuesDo { |key value|
				anNdef.addSpec(key, value)
			};
			anNdef.gui.parent.onClose_ {
				anNdef.clearHalo
			};
			ProxyPresetGui.new(NdefPreset.new(aSymbol)) /* JITLibExtensions */
		}

		*connectMidiOut { |recvFunc|
			MIDIClient.initialized.not.if {
				MIDIClient.init
			};
			Ui.chooseFromList(MIDIClient.externalDestinations) { |ix|
				ix.notNil.if {
					var midiOut = MIDIOut(0);
					midiOut.connect(ix);
					recvFunc.value(midiOut)
				}
			}
		}

	}

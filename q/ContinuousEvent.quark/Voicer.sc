Voicer {

	//--------

	*ar { |part=0 numVoices=16 voiceFunc|
		^ContinuousEventManager.voicer(part, numVoices, voiceFunc)
	}

	*new { |part=0 numVoices=16 voiceFunc|
		^ContinuousEventManager.voicer(part, numVoices, voiceFunc)
	}

}

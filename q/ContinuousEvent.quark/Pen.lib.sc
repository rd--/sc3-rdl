// Kyma pen names.  Voice number are one indexed.  Control value are 0-1.
PenDown {
	*new { |voiceNumber=1|
		^In.kr(ContinuousEventManager.voiceAddr(0, voiceNumber - 1) + 0, 1)
	}
}

PenX {
	*new { |voiceNumber=1|
		^In.kr(ContinuousEventManager.voiceAddr(0, voiceNumber - 1) + 1, 1)
	}
}

PenY {
	*new { |voiceNumber=1|
		^In.kr(ContinuousEventManager.voiceAddr(0, voiceNumber - 1) + 2, 1)
	}
}

PenZ {
	*new { |voiceNumber=1|
		^In.kr(ContinuousEventManager.voiceAddr(0, voiceNumber - 1) + 3, 1)
	}
}

PenAngle {
	*new { |voiceNumber=1|
		^In.kr(ContinuousEventManager.voiceAddr(0, voiceNumber - 1) + 4, 1)
	}
}

PenRadius {
	*new { |voiceNumber=1|
		^In.kr(ContinuousEventManager.voiceAddr(0, voiceNumber - 1) + 5, 1)
	}
}

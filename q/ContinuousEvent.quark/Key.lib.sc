// Kyma keyboard names. Voice number are one indexed. Control value are 0-1.
KeyDown {
	*new { |voiceNumber=1|
		^In.kr(ContinuousEventManager.voiceAddr(0, voiceNumber - 1) + 0, 1)
	}
}

KeyTimbre {
	*new { |voiceNumber=1|
		^In.kr(ContinuousEventManager.voiceAddr(0, voiceNumber - 1) + 2, 1)
	}
}

KeyVelocity {
	*new { |voiceNumber=1|
		^In.kr(ContinuousEventManager.voiceAddr(0, voiceNumber - 1) + 3, 1)
	}
}

KeyPitch {
	*new { |voiceNumber=1|
		^In.kr(ContinuousEventManager.voiceAddr(0, voiceNumber - 1) + 7, 1)
	}
}

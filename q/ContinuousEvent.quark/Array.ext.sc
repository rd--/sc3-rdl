+Array {

	unitCps {
		^(this * 100).midicps
	}

	unitMidi {
		^this * 100
	}

}

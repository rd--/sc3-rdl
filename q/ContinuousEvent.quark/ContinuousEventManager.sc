ContinuousEventManager {

	//--------

	classvar <>addrZero=13000, <>maxEventParam=10, <>maxVoices=24;

	*copyMidi { |part=0 numVoices=16|
		var server = Server.default;
		var nextVoice = 0;
		var voiceTable = Array.newClear(128); // keyNumber -> voice
		MIDIIn.noteOn = { |sourceId channel keyNumber keyVelocity|
			var baseIndex = this.voiceAddr(part, nextVoice);
			server.sendMsg('/c_set', baseIndex + 0, 1); // w
			server.sendMsg('/c_set', baseIndex + 3, keyVelocity / 127); // z
			server.sendMsg('/c_set', baseIndex + 7, keyNumber); // p
			voiceTable[keyNumber] = nextVoice;
			nextVoice = nextVoice + 1 % numVoices
		};
		MIDIIn.noteOff = { |sourceId channel keyNumber keyVelocity|
			var voice = voiceTable[keyNumber];
			server.sendMsg('/c_set', this.voiceAddr(part, voice) + 0, 0) // w
		}
	}

	*initialize { |numParts=8 numVoices=16|
		var param = ContinuousEvent.new.init;
		var server = Server.default;
		numParts.do { |part|
			numVoices.do { |voice|
				param.part = part;
				param.voice = voice;
				server.listSendMsg(param.setAllMsg)
			}
		}
	}

	*partAddr { |part=0|
		^addrZero + (part * maxVoices * maxEventParam)
	}

	*player { |part=0 numVoices=16 eventSource|
		var server = Server.default;
		var nextVoice = 0;
		var atEnd = false;
		var routineFunc = {
			{
				atEnd.not
			}.while {
				var event = eventSource.value(()); // empty event required for Pbind/Stream
				atEnd = event.isNil;
				atEnd.not.if {
					var dur = event.dur ? 0.1;
					var legato = event.legato ? 1.0;
					event.multiChannelExpand.do { |e|
						var p = ContinuousEvent.new.fromEvent(part, nextVoice, e);
						server.listSendMsg(p.setAllMsg);
						thisThread.clock.sched(dur * legato) {
							server.listSendMsg(p.setGateMsg(0))
						};
						nextVoice = nextVoice + 1 % numVoices
					};
					dur.wait
				}
			}
		};
		^routineFunc.fork
	}

	*voiceAddr { |part=0 voice=0|
		^this.partAddr(part) + (voice * maxEventParam)
	}

	*voicer { |part=0 numVoices=16 voiceFunc|
		^(0 .. numVoices - 1).collect { |voice|
			voiceFunc.value(
				ContinuousEvent.new.fromArray(part, voice, In.kr(bus: this.voiceAddr(part, voice), numChannels: 8))
			)
		}
	}

	*writer { |part=0 numVoices=16 voiceFunc|
		^0.to(numVoices - 1).collect { |voice|
			var e = ContinuousEvent.new;
			e.part = part;
			e.voice = voice;
			voiceFunc.value(e);
			Out.kr(ContinuousEventManager.voiceAddr(voice), e.asArray.kr)
		}
	}

}

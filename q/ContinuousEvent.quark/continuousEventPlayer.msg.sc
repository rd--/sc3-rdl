// .alias for ContinuousEventManager.player

+ Function {

	continuousEventPlayer { |numVoices|
		^ContinuousEventManager.player(numVoices, this)
	}

}

+ Routine {

	continuousEventPlayer { |numVoices|
		^ContinuousEventManager.player(numVoices, this)
	}

}

+ Pbind {

	continuousEventPlayer { |numVoices|
		^ContinuousEventManager.player(numVoices, this.asStream)
	}

}

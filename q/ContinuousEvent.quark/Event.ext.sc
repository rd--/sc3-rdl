+Event {

	flop {
		^this.multiChannelExpand
	}

	multiChannelExpand {
		var keys = this.keys.asArray;
		var values = keys.collect { |item|
			this[item]
		};
		var size = values.collect { |each|
			each.size
		}.maxItem;
		var places = values.flop.collect { |anArray|
			anArray.collect { |item index|
				keys[index] -> item
			}
		};
		^places.collect { |item|
			item.inject(()) { |accum each|
				accum.add(each)
			}
		}
	}

}

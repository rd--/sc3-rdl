+SimpleNumber {

	unitCps {
		^this.unitMidi.midicps
	}

	unitMidi {
		^this * 100
	}

}

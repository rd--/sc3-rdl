VoiceWriter {

	//--------

	*new { |numVoices voiceFunc|
		^ContinuousEventManager.writer(numVoices, voiceFunc)
	}

}

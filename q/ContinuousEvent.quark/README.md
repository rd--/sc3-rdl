# ContinuousEvent

A continuous event is a collection of control signals with an associated gate parameter.

All control values are in (0, 1).

By convention:

- w = gate
- x = horizontal axis
- y = vertical axis
- z = depth or pressure axis

Pen parameters:

- i = orientation
- j = contact ellipse x-axis
- k = contact ellipse y-axis

Derived parameters:

- p = pitch, by convention 1/100th of a midi note number (eg. 60 => 0.6)
- x? = proportional distance on x-axis of centre of p polygon
- y? = proportional distance on y-axis of centre of p polygon

ContinuousEvent {

	var <>part=0; // Integer
	var <>voice=0; // Integer
	var <>contents; // Array

	w { ^contents[0] }
	x { ^contents[1] }
	y { ^contents[2] }
	z { ^contents[3] }
	i { ^contents[4] }
	j { ^contents[5] }
	k { ^contents[6] }
	p { ^contents[7] }

	asArray {
		^contents
	}

	asEvent {
		^(
			w: this.w,
			x: this.x,
			y: this.y,
			z: this.z,
			i: this.i,
			j: this.j,
			k: this.k,
			p: this.p
		)
	}

	fromArray { |part=0 voice=0 paramArray|
		this.part = part;
		this.voice = voice;
		contents = paramArray
	}

	fromAssocArray { |assocArray|
		var e = Event.new;
		assocArray.do { |item|
			e[item.key.asSymbol] = item.value
		};
		this.fromEvent(voice, e)
	}

	fromEvent { |part=0 voice=0 paramEvent|
		this.part = part;
		this.voice = voice;
		contents[0] = paramEvent.w ? contents[0];
		contents[1] = paramEvent.x ? contents[1];
		contents[2] = paramEvent.y ? contents[2];
		contents[3] = paramEvent.z ? contents[3];
		contents[4] = paramEvent.i ? contents[4];
		contents[5] = paramEvent.j ? contents[5];
		contents[6] = paramEvent.k ? contents[6];
		contents[7] = paramEvent.p ? contents[7];
	}

	init {
		part = 0;
		voice = 0;
		contents = [
			/* w */ 0,
			/* x y z */ 0.5, 0.5, 0.1,
			/* i j k */ 0.5, 0.5, 0.5,
			/* p */ 0.60
		]
	}

	setAllMsg {
		^["/c_setn", ContinuousEventManager.voiceAddr(part, voice), contents.size] ++ contents
	}

	setGateMsg { |aNumber|
		// Store gate value and send message to synthesiser.
		contents[0] = aNumber;
		^["/c_set", ContinuousEventManager.voiceAddr(part, voice), aNumber]
	}

	//--------

	*new {
		^super.new.init
	}

}

ContinuousEventUi {

	var <>areaHeight; // Double (mm)
	var <>areaScaling; // Double
	var <>areaWidth; // Double (mm)
	var <>deriveXFromP; // Boolean
	var <>eventDrawIndexText; // Bool
	var <>eventDrawPitchText; // Bool
	var <>frameRate; // Number
	var <>gridDrawCircle; // Bool
	var <>gridDrawPoint; // Bool
	var <>gridDrawPolygon; // Bool
	var <>gridDrawText; // Bool
	var <>gridDrawXAxis; // Bool
	var <>gridDrawYAxis; // Bool
	var <>gridFileName; // String

	init { |frameRate areaWidth areaHeight areaScaling gridFileName deriveXFromP|
		this.frameRate = frameRate;
		this.areaWidth = areaWidth;
		this.areaHeight = areaHeight;
		this.areaScaling = areaScaling;
		this.gridFileName = gridFileName;
		this.deriveXFromP = deriveXFromP;
		this.eventDrawIndexText = false;
		this.eventDrawPitchText = false;
		this.gridDrawXAxis = true;
		this.gridDrawYAxis = true;
		this.gridDrawPoint = true;
		this.gridDrawCircle = true;
		this.gridDrawText = true;
		this.gridDrawPolygon = true
	}

	makeWindow {
		var s = Server.default;
		var optAspect = 'i'; // aspect ratio: i|x|y
		var xMul = (optAspect == 'y').if {
			areaHeight * areaScaling
		} {
			areaWidth * areaScaling
		};
		var yMul = (optAspect == 'x').if {
			areaWidth * areaScaling
		} {
			areaHeight * areaScaling
		};
		var xToScr = { |x|
			x * xMul
		};
		var yToScr = { |y|
			yMul - (y * yMul)
		};
		var ptToScr = { |p|
			xToScr.(p.x) @ yToScr.(p.y)
		};
		/* [[0,0,0.5,0.5,60.0,1.0,1.0,"C4",0]] */
		var gridDat = gridFileName.isNil.if {
			[]
		} {
			CSVFileReader.readInterpret(
				path: gridFileName,
				skipEmptyLines: false,
				skipBlanks: false,
				delimiter: $,
			)
		};
		var gridN = gridDat.size;
		var gridDatT = gridDat.flop;
		var gridX = gridDatT[2];
		var gridY = gridDatT[3];
		var gridP = (gridN > 0).if {
			(0 .. gridN - 1).collect { |i|
				gridX[i] @ gridY[i]
			}
		};
		var gridW = gridDatT[5];
		var gridH = gridDatT[6];
		var gridTxt = gridDatT[7];
		var gridXLoc = gridX.as(Set).as(Array).sort;
		var gridYLoc = gridY.as(Set).as(Array).sort;
		var gridLnDat = gridDat.collect { |e|
			(e.size > 8).if {
				e.copyRange(start: 8, end: e.size - 1).clump(2).collect { |p|
					p[0] @ p[1]
				}
			} {
				[]
			}
		};
		var evtK0 = 13000;
		var evtParts = 16;
		var evtElem = 8;
		var evtIncr = 10;
		var evtData = Array.fill(
			size: evtParts,
			function: {
				Array.fill(8, { 0.0 })
			}
		);
		var b = Bus(
			rate: \control,
			index: evtK0,
			numChannels: evtParts * evtIncr,
			server: s
		);
		var wn = Window(
			name: "ContinuousEventUi",
			bounds: Rect(100, 100, areaWidth * areaScaling, areaHeight * areaScaling),
			resizable: false
		);
		var recvFunc = { |busData|
			evtParts.do { |i|
				evtData[i] = busData.copyRange(i * evtIncr,i * evtIncr + evtElem)
			};
			{
				wn.refresh
			}.defer
		};
		var drawF = {
			var rMul = 25;
			(gridN > 0).if {
				gridDrawXAxis.if {
					Pen.strokeColor_(Color.grey(grey: 0.65,alpha: 0.5));
					gridXLoc.do { |x|
						Pen.moveTo(xToScr.(x) @ 0);
						Pen.lineTo(xToScr.(x) @ yToScr.(1));
						Pen.stroke
					}
				};
				gridDrawYAxis.if {
					Pen.strokeColor_(Color.grey(grey: 0.65,alpha: 0.5));
					gridYLoc.do { |y|
						Pen.moveTo(0 @ yToScr.(y));
						Pen.lineTo(xToScr.(1) @ yToScr.(y));
						Pen.stroke
					}
				};
				gridDrawPoint.if {
					Pen.fillColor_(Color.blue(val: 0.85,alpha: 0.25));
					gridP.do { |p|
						var r = 2.5;
						var rect = Rect(
							left: xToScr.(p.x) - r,
							top: yToScr.(p.y) - r,
							width: r * 2,
							height: r * 2
						);
						Pen.fillOval(rect)
					}
				};
				gridDrawCircle.if {
					Pen.strokeColor_(Color.grey(grey: 0.35,alpha: 0.25));
					(0 .. gridN - 1).do { |i|
						var p = gridP[i];
						var w = gridW[i] * xMul;
						var h = gridH[i] * yMul;
						var rect = Rect(
							left: xToScr.(p.x) - (w / 2),
							top: yToScr.(p.y) - (h / 2),
							width: w,
							height: h
						);
						Pen.strokeOval(rect)
					}
				};
				gridDrawText.if {
					Pen.strokeColor_(Color.grey(grey: 0.35, alpha: 0.25));
					(0 .. gridN - 1).do { |i|
						var p = gridP[i];
						var w = gridW[i] * xMul;
						var h = gridH[i] * yMul;
						var txt = gridTxt[i];
						var rect = Rect(
							left: xToScr.(p.x) - (w / 2),
							top: yToScr.(p.y) - 24,
							width: w,
							height: 12
						);
						Pen.stringCenteredIn(
							string: txt,
							rect: rect,
							font: Font.sansSerif(size: 9),
							color: Color.black
						)
					}
				};
				gridDrawPolygon.if {
					Pen.strokeColor_(Color.grey(grey: 0.65,alpha: 0.5));
					gridLnDat.do { |c|
						(c.size > 0).if {
							Pen.moveTo(ptToScr.(c[c.size - 1]));
							(0 .. c.size - 1).do { |i|
								Pen.lineTo(ptToScr.(c[i]))
							};
							Pen.stroke
						}
					}
				}
			};
			evtData.do { |e k|
				(e[0] > 0).if {
					var x = xToScr.(deriveXFromP.if {
						e[7]
					} {
						e[1]
					});
					var y = yToScr.(e[2]);
					var r = 5 + (e[3] * rMul);
					var rect = Rect(left: x - r, top: y - r, width: r * 2, height: r * 2);
					var rx = 0.5 + (e[5] * 35);
					var ry = 0.5 + (e[6] * 35);
					Pen.strokeColor_(Color.black);
					Pen.strokeOval(rect);
					eventDrawPitchText.if {
						var textRect = Rect(
							left: x - (rMul * 3),
							top: y - (rMul * 2),
							width: rMul * 6,
							height: rMul * 2
						);
						Pen.stringCenteredIn(
							string: (e[7] + e[8]).round(0.01).asString,
							rect: textRect,
							font: Font.sansSerif(size: 9),
							color: Color.black
						)
					};
					eventDrawIndexText.if {
						var textRect = Rect(
							left: x - (rMul * 3),
							top: y + (rMul * 0.25),
							width: rMul * 6,
						height: rMul * 2);
						Pen.stringCenteredIn(
							string: k.asString,
							rect: textRect,
							font: Font.sansSerif(size: 11),
							color: Color.black
						)
					};
					Pen.use {
						Pen.rotate(e[4] * 2 * pi, x, y);
						Pen.strokeRect(rect);
						Pen.moveTo((x - (rx / 2)) @ y);
						Pen.lineTo((x + (rx / 2)) @ y);
						Pen.stroke;
						Pen.moveTo(x @ (y + (ry / 2)));
						Pen.lineTo(x @ (y - (ry / 2)));
						Pen.stroke
					}
				}
			}
		};
		var getRt = SkipJack(
			updateFunc: {
				b.get(recvFunc)
			},
			dt: 1 / frameRate,
			stopTest: false,
			name: "ContinuousEventUi",
			clock: TempoClock,
			autostart: true
		);
		wn.background = Color.fromHexString("#FFFFE8");
		wn.drawFunc = drawF;
		wn.onClose = {
			getRt.stop
		};
		wn.front
	}

	//--------

	*new { |frameRate = 25, areaWidth = 230, areaHeight = 130, areaScaling = 2, gridFileName = nil, deriveXFromP = false|
		^super.new.init(frameRate, areaWidth, areaHeight, areaScaling, gridFileName, deriveXFromP);
	}

}

# Terse Guide

Interpret and summarise Terse Guides.

See [TerseGuide.help.scd](https://gitlab.com/rd--/sc3-rdl/-/blob/master/q/TerseGuide.quark/TerseGuide.help.scd)

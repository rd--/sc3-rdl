+String {

	paragraphs {
		^this.paragraphsLines.collect { |each|
			each.join($\n)
		}
	}

	paragraphsLines {
		^this.split($\n).separate { |a b|
			a.isEmpty
		}.collect { |paragraph|
			paragraph.reject { |line|
				line.isEmpty
			}
		}
	}

	terseGuideSummary {
		var text = File.readAllString(this);
		var areas = text.paragraphsLines;
		var totalTestCount = 0;
		var totalPassCount = 0;
		('Terse Guide Summary: Areas = ' ++ areas.size.asString).postln;
		areas.do { |entries|
			entries[0].postln;
			(entries.size > 1 && { entries[0].contains(" - !").not }).if {
				var testCount = entries.size - 1;
				var passCount = 0;
				(1 .. testCount).collect { |index|
					var test = entries[index];
					var answer = test.interpret;
					(answer == true).if {
						passCount = passCount + 1;
					} {
						('	Error: ' ++ test).postln
					}
				};
				totalTestCount = totalTestCount + testCount;
				totalPassCount = totalPassCount + passCount;
				('	=> ' ++ passCount.asString ++ ' / ' ++ testCount.asString).postln
			}
		};
		('Total => ' ++ totalPassCount.asString ++ ' / ' ++ totalTestCount.asString).postln
	}

}

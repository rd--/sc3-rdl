# PolyglotBrowser

Polygot graph browser on directory:

	PolyglotGraphBrowser.on(directoryName: "/home/rohan/sw/hsc3-graphs/db/")

See [PolyglotBrowser.help.scd](https://gitlab.com/rd--/sc3-rdl/-/blob/master/q/PolyglotBrowser.quark/PolyglotBrowser.help.scd)

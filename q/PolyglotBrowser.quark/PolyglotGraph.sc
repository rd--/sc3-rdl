PolyglotGraph {

	var <>fileId; // Symbol
	var <>fileName; // String
	var <>lang; // Symbol
	var <>scsyndefData; // Int8Array
	var <>scsyndefFileName; // String
	var <>sourceCode; // String

	init { |fileName|
		this.fileName = fileName;
		this.fileId = fileName.fileNameWithoutExtension.asSymbol;
		this.lang = fileName.fileNameLang;
		this.sourceCode = File.readAllString(fileName);
		this.scsyndefFileName = fileName.fileNameReplaceExtension("scsyndef");
		this.scsyndefData = scsyndefFileName.loadScSynDefFile;
	}

	//--------

	*new { |fileName|
		^super.new.init(fileName);
	}

}

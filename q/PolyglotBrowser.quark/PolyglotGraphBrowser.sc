PolyglotGraphBrowser {

	var <>db; // [PolyglotGraph]
	var <>directoryNames; // String
	var <>langExtensions; // [String]
	var <>selected; // PolyglotGraph|nil
	var <>server; // NetAddr

	allId {
		^db.collect('fileId');
	}

	init { |directoryNames langExtensions|
		var dbFileSet = directoryNames.collect { |dir|
			dir.directoryFilesWithExtensionIn(langExtensions);
		}.flatten;
		this.directoryNames = directoryNames;
		this.langExtensions = langExtensions;
		this.db = dbFileSet.collect { |fileName|
			PolyglotGraph(fileName: fileName)
		};
		this.selected = db[0];
		this.server = Server.default;
	}

	lookupId { |fileId|
		^db.detect { |p|
			p.fileId == fileId
		};
	}

	makeWindow {
		var w = Window(bounds: Rect(128, 64, 900, 600)).front;
		var tSearch = TextField(w);
		var cFs = CheckBox(parent: w, text: "fs").value_(true);
		var cHs = CheckBox(parent: w, text: "hs").value_(true);
		var cScala = CheckBox(parent: w, text: "scala").value_(true);
		var cSch = CheckBox(parent: w, text: "sch").value_(true);
		var cScm = CheckBox(parent: w, text: "scm").value_(true);
		var cSt = CheckBox(parent: w, text: "st").value_(true);
		var cSl = CheckBox(parent: w, text: "sl").value_(true);
		var cScd = CheckBox(parent: w, text: "scd").value_(true);
		var cLang = [cFs, cHs, cScala, cSch, cScm, cScd, cSl, cSt];
		var tNgraphs = StaticText(parent: w).string_("# Graphs:" + db.size.asString);
		var inSelectedLang = { |x|
			switch(x,
				'forth', { cFs.value },
				'haskell', { cHs.value },
				'scala', { cScala.value },
				'schemehaskell', { cSch.value },
				'scheme', { cScm.value },
				'simplelanguage', { cSl.value },
				'smalltalk', { cSt.value },
				'supercollider', { cScd.value },
				{ false });
			};
			var tSearchAppendUgen = { |ugen|
				tSearch.string_((tSearch.string + ugen.name).stripWhiteSpace);
				tSearch.doAction;

			};
			var bUgen = Ui.button(parent: w, text: "Ugen", recvFunc: {
				Ui.chooseUgenWithInformation(selFunc: {true}, recvFunc: tSearchAppendUgen)
			}).maxWidth_(40);
			// String -> String -> Bool
			var txtContainsAllTerms = { |txt terms|
				var inTxt = { |x|
					txt.find(x, ignoreCase:true).notNil
				};
				terms.ifEmpty {
					true
				} {
					terms.split($ ).every(inTxt)
				}
			};
			// PolyglotGraph -> String
			var mkEntryText = { |p|
				p.fileId.asString + p.sourceCode.split($\n)[0]
			};
			var tFont = Font.defaultMonoFace;
			// var tFront = Font(name: "Monospace", size: 11, bold: false, italic: false, usePointSize: false)
			var lUniv = ListView(w).items_(db.collect(mkEntryText));
			var tCode = TextView(w).minHeight_(12 * 12).font_(tFont);
			var doPlay = {
				server.sendMsg("/s_new", selected.fileId, server.nextNodeID, 0, 1)
			};
			var doStop = {
				CmdPeriod.run
			};
			var doReplace = {
				server.freeAll;
				{ doPlay.value }.defer(0.1)
			};
			var doDraw = {
				var cmd = "hsc3-dot scsyndef-draw" + selected.scsyndefFileName;
				cmd.unixCmd
			};
			var doGetStat = {
				var cmd = "hsc3-scsynth scsyndef stat" + selected.scsyndefFileName;
				cmd.unixCmdGetStdOut
			};
			var doStat = {
				var w = Window(resizable:false, bounds:Rect(128, 64, 900, 300)).front;
				var t = TextView(w).string_(doGetStat.value);
				w.layout = VLayout(t);
			};
			var lookupTextureParam = {
				var res = selected.sourceCode.findRegexp("texture=[., a-z0-9]*");
				(res == []).if {
					"overlap, 4, 4, 4, inf"
				} {
					res[0][1].drop(8)
				}
			};
			var doTexture= {
				var syName = selected.fileId;
				var txParam = lookupTextureParam.value;
				var syFilename = selected.scsyndefFileName;
				var w = Window(bounds: Rect(120, 480, 500, 20), resizable:false).front;
				var tSyName = StaticText(parent: w).string_(syName);
				var tTxParam = TextField(parent: w).string_(txParam);
				var pProc = nil;
				var procEnd = {
					"procEnd".postln;
					pProc.ifNotNil {
						pProc.putChar($q);
						pProc.close;
						pProc = nil
					}
				};
				var doControl = { |x|
					var cmd = "hsc3-lang scsyndef-to-texture" + syFilename + tTxParam.string;
					(x.value == 1).if {
						pProc = Pipe(commandLine: cmd, mode: "w")
					} {
						procEnd.value
					}
				};
				var startStopSt = [["Start", Color.black, Color.white], ["Stop", Color.black, Color.white]];
				var bControl = Button(w).states_(startStopSt).action_(doControl);
				w.layout_(HLayout(tSyName, tTxParam, bControl)).onClose_(procEnd)
			};
			var doUiCtl = {
				var cmd = "hsc3-ui ctl scsyndef" + selected.scsyndefFileName;
				cmd.unixCmd
			};
			var doEventUi = {
				REventUI(frameRate: 60, areaWidth: 230, areaHeight: 130, areaScaling: 2.5, gridFileName: nil).makeWindow
			};
			var doUnivSel = { |i e_|
				var txt = lUniv.items[i.value];
				txt.isNil.if {
					selected = nil;
					tCode.string = "";
				} {
					var sym = txt.split($ )[0].asSymbol;
					selected = this.lookupId(sym);
					tCode.string = selected.sourceCode;
					server.sendMsg("/d_recv", selected.scsyndefData)
				};
			};
			var doFilter = {
				var narrow = db.select { |p|
					inSelectedLang.value(p.lang) && txtContainsAllTerms.value(p.sourceCode, tSearch.string)
				};
				lUniv.items_(narrow.collect(mkEntryText));
				tNgraphs.string_("# Graphs:" + narrow.size.asString);
				doUnivSel.value(0);
			};
			var filterOnKeyUp = false;
			var lUnivMenu = [
				MenuAction("Play", doPlay).shortcut_("Ctrl+P"),
				MenuAction("Stop", doStop).shortcut_("Ctrl+."),
				MenuAction("Replace", doReplace).shortcut_("Ctrl+R"),
				MenuAction("Draw", doDraw).shortcut_("Ctrl+D"),
				MenuAction("Stat", doStat).shortcut_("Ctrl+S"),
				MenuAction("Texture", doTexture).shortcut_("Ctrl+T"),
				MenuAction("UI:Event", doEventUi).shortcut_("Ctrl+E"),
				MenuAction("UI:Ctl", doUiCtl).shortcut_("Ctrl+U")
			];
			lUniv.action_(doUnivSel);
			lUniv.setContextMenuActions(*lUnivMenu);
			w.layout = VLayout(HLayout(bUgen, tSearch, cScd, cSl, cSt, cHs, cSch, cScm, cFs, cScala, tNgraphs), lUniv, tCode);
			cLang.collect { |c|
				c.action_(doFilter)
			};
			filterOnKeyUp.ifTrue {
				tSearch.keyUpAction_(doFilter)
			};
			tSearch.action_(doFilter);
		}

		//--------

		*on { |directoryName|
			this.onArray([directoryName])
		}

		*onArray { |directoryNames|
			this.new(directoryNames).makeWindow
		}

		*new { |directoryNames|
			^super.new.init(directoryNames, ["fs", "hs", "scd", "sch", "scm", "sl", "st", "scala"]);
		}

	}

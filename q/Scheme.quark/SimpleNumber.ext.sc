+SimpleNumber {

	iota { |start = 0, step = 1|
		^start.series(start + step, start + (this - 1 * step))
	}

}

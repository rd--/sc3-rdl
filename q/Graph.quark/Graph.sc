Graph {

	var <vertices, <edges; // Set

	addEdge { |from, to|
		^this.subclassResponsibility
	}

	addVertex { |vertex|
		vertices.add(vertex)
	}

	allNeighbours {
		^vertices.collect { |item|
			item -> this.neighboursOf(item)
		}
	}

	degree {
		^[vertices.size, edges.size]
	}

	hasEdge { |from to|
		^this.subclassResponsibility
	}

	inEdgesOf { |vertex|
		^edges.select { |edge|
			edge[1] == vertex
		}
	}

	init {
		vertices = Set.new;
		edges = Set.new
	}

	isNormal {
		^vertices.asArray.sort == (0 .. vertices.size - 1)
	}

	neighboursOf { |vertex|
		^this.outEdgesOf(vertex).collect('second')
	}

	outEdgesOf { |vertex|
		^edges.select { |edge|
			edge[0] == vertex
		}
	}

	printDotOn { |graphType edgeType aStream|
		aStream << graphType << " g { ";
			vertices.do { |vertex|
				aStream << vertex << ";"
			};
			edges.do { |edge|
				aStream << edge[0] << " " << edgeType << " " << edge[1] << ";"
			};
		aStream << "}"
	}

	random { |vertexCount edgeCount|
		0.to(vertexCount - 1).do { |item|
			this.addVertex(item)
		};
		edgeCount.do {
			this.addEdge(vertexCount.rand, vertexCount.rand)
		}
	}

	showDot {
		File.use("/tmp/graph.dot", "w") { |aStream|
			this.printDotOn(aStream)
		};
		"dot-xlib.sh /tmp/graph.dot".systemCmd
	}

	//--------

	*new {
		^super.new.init
	}

}

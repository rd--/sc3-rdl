UGraph : Graph {

	addEdge { |from to|
		edges.add([from, to].sort)
	}

	hasEdge { |from to|
		var edge = [from, to].sort;
		^edges.any { |item|
			item == edge
		}
	}

	printDotOn { |aStream|
		super.printDotOn("graph", "--", aStream)
	}

	showDot {
		super.showDot("graph", "->")
	}

}

DiGraph : Graph {

	addEdge { |from to|
		edges.add([from, to])
	}

	hasEdge { |from to|
		var edge = [from, to];
		^edges.any { |item|
			item == edge
		}
	}

	printDotOn { |aStream|
		super.printDotOn("digraph", "->", aStream)
	}

	showDot {
		super.showDot("digraph", "->")
	}

}

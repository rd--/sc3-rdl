# Graph

Implements _Graph_, _DiGraph_ and _UGraph_.

Vertices are integers, edges are two-element arrays of vertices.

Note: edges are not Associations because (0->1) == (0->2) [St-80].

# UgenDb

Utility methods used when constructing the Haskell Ugen database.

See [UgenDb.help.scd](https://gitlab.com/rd--/sc3-rdl/-/blob/master/q/UgenDb.quark/UgenDb.help.scd)

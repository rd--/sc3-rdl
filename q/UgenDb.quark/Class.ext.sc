+Class {

	defaultRate {
		var allRates = this.supportedRates;
		allRates.isEmpty.if {
			^'ir'
		} {
			^allRates[0]
		}
	}

	inputDefault { |rate name|
		var method = this.locateMethod(rate);
		var names = method.argNames;
		var index = names.detectIndex { |each|
			each == name
		};
		var default = method.prototypeFrame[index];
		default.isNumber.if {
			^default
		} {
			^0
		}
	}

	inputNames { |rate|
		var names = this.locateMethod(rate).argNames;
		var ignored = ['this', 'mul', 'add', 'selector']; // uop & binop
		^names.reject { |each|
			ignored.includes(each)
		}
	}

	isDemand {
		^[Dibrown, Diwhite, Dswitch, Dunique].includes(this) || [ListDUGen, DUGen].includes(this.superclass)
	}

	isExtension {
		^this.filenameSymbol.asString.find("SCClassLibrary/Common") == nil
	}

	isLocal {
		^this.isExtension.not
	}

	isPure {
		^this.superclassesOf.indexOf(PureUGen) != nil
	}

	isPv {
		var name = this.asString;
		^(name == "FFT") || (name.find("PV_") == 0)
	}

	numberOfOutputs { |rate|
		/* https://github.com/supercollider/supercollider/issues/232 */
		var answer;
		this.name.switch(
			'BeatTrack', { ^4 },
			'InRect', { ^1 },
			'DbufTag', { ^1 },
			'Dfsm', { ^1 },
			'Dtag', { ^1 },
			'LPCSynth', { ^1 },
			'LPCVals', { ^1 },
			'Rotate', { ^4 },
			'Tilt', { ^4 },
			'Tumble', { ^4 }
		);
		try {
			var ugen = this.perform(rate);
			ugen.isArray.if {
				answer = ugen.size
			} {
				ugen.isFloat.if {
					answer = 0
				} {
					answer = 1
				}
			}
		} { |err|
			answer = 1
		};
		^answer
	}

	defaultConstructor {
		var allRates = this.supportedRates;
		(this.isDemand || this.isPv || allRates.isEmpty).if {
			^'new'
		} {
			^this.defaultRate
		}
	}

	supportedRates {
		^['ar', 'kr', 'dr', 'ir'].select { |each|
			this.supportsRate(each)
		}
	}

	supportsRate { |rate|
		[BinaryOpUGen, UnaryOpUGen].includes(this).if {
			^true
		} {
			^(this.isDemand && (rate == 'dr')) ||
			 (this.isPv && (rate == 'kr')) ||
			 (this.locateMethod(rate) != nil)
		}
	}

	ugenDbEntry {
		var name = this.name.asString;
		var cons = this.defaultConstructor;
		var inputs = this.inputNames(cons).collect { |each|
			[
				each.asString,
				this.inputDefault(cons, each)
			]
		};
		^[
			name,
			this.supportedRates,
			this.defaultRate,
			inputs,
			this.numberOfOutputs(cons),
			this.ugenSummary,
			this.isPure
		]
	}

	ugenSummary {
		var help = SCDoc.documents["Classes/" ++ this.name.asString];
		(help == nil).if {
			^"No summary"
		} {
			^help.summary
		}
	}

}

+String {

	fileNameExtension {
		^PathName(this).extension
	}

	fileNameWithoutExtension {
		^PathName(this).fileNameWithoutExtension
	}

	fileNameDirectory {
		^PathName(this).pathOnly
	}

	fileNameReplaceExtension { |extension|
		var pathName = PathName(this);
		^(pathName.pathOnly +/+ pathName.fileNameWithoutExtension ++ "." ++ extension)
	}

	fileNameHasExtensionIn { |extensionArray|
		^extensionArray.includesEqual(this.fileNameExtension)
	}

	fileNameLang {
		var ext = this.fileNameExtension;
		^switch(ext,
			"fs", { 'forth' },
			"hs", { 'haskell' },
			"lisp", { 'lisp' },
			"scala", { 'scala' },
			"scd", { 'supercollider' },
			"sch", { 'schemehaskell' },
			"scm", { 'scheme' },
			"sl", { 'simplelanguage' },
			"st", { 'smalltalk' },
			"stc", { 'smalltalkc' },
			{ 'unknown' }
		)
	}

	isSynthdefFile {
		^this.fileNameHasExtensionIn(["scsyndef"])
	}

	directoryFiles {
		^PathName(this).files().collect { |each|
			each.fullPath
		}
	}

	directoryFilesRecursive {
		var filesList = List(24);
		PathName(this).filesDo { |filePath|
			filesList.add(filePath.fullPath)
		};
		^filesList.asArray
	}

	directoryFilesWithExtensionIn { |extensionArray|
		^this.directoryFiles.selectFilesWithExtensionIn(extensionArray)
	}

	directoryFilesRecursiveWithExtensionIn { |extensionArray|
		^this.directoryFilesRecursive.selectFilesWithExtensionIn(extensionArray)
	}

	directoryFindFile { |fileName|
		PathName(this).filesDo { |filePath|
			(filePath.fileName == fileName).if {
				^filePath.fullPath
			}
		};
		^nil
	}

	findHelpFileFor {
		var helpFileName = this ++ ".help.scd";
		var directoryArray = LanguageConfig.includePaths;
		directoryArray.do { |directoryName|
			var fileName = directoryName.directoryFindFile(helpFileName);
			fileName.notNil.if {
				^fileName
			}
		};
		^nil
	}

	/* PathName -> Int8Array */
	loadScSynDefFile {
		var fd = File.open(this, "rb");
		var sz = File.fileSize(this);
		var ra = Int8Array.newClear(sz);
		var qr = fd.read(ra);
		fd.close
		^ra
	}

	openHelpFileFor {
		var fileName = this.findHelpFileFor;
		fileName.notNil.if {
			fileName.openDocument
		} {
			("no help file found: " ++ this).postln
		}
	}

	readSf1 {
		^SoundFile.use(this) { |fd|
			(fd.numChannels == 1).if {
				var arr = FloatArray.newClear(fd.numFrames);
				fd.readData(arr);
				arr
			} {
				"String>>readSf1: not mono".error
			}
		}
	}

	userFile {
		var pathName = Platform.userHomeDir +/+ this;
		File.existsCaseSensitive(pathName).not.if {
			("String>>userFile: does not exist: " ++ pathName).error
		} {
			^pathName
		}
	}

	/* String -> (() -> Ugen) -> String -> () */
	writeScSynDef { |graphFunc outputDirectory|
		graphFunc.asSynthDef(name: this).writeDefFile(outputDirectory)
	}

}

+Array {

	selectFilesWithExtensionIn { |extensionArray|
		^this.select { |fileName|
			fileName.fileNameHasExtensionIn(extensionArray)
		}
	}

}

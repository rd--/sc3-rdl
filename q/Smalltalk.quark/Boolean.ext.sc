+Boolean {

	ifFalse { |falseFunc|
		^this.not.if(falseFunc)
	}

	ifFalseIfTrue { |falseFunc trueFunc|
		^this.not.if(falseFunc, trueFunc)
	}

	ifTrue { |trueFunc|
		^this.if(trueFunc)
	}

	ifTrueIfFalse { |trueFunc ifFalse|
		^this.if(trueFunc, ifFalse)
	}

	isBoolean {
		^true
	}

}

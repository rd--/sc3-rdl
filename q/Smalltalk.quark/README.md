# Smalltalk

Methods from Smalltalk and Squeak.

See [Smalltalk.help.scd](https://gitlab.com/rd--/sc3-rdl/-/blob/master/q/Smalltalk.quark/Smalltalk.help.scd)

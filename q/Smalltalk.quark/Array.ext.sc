+Array {

	asArray {
		^this
	}

	atRandom {
		^this.choose
	}

	bitShiftRight { |operand|
		^this.rightShift(operand)
	}

	negated {
		^this.neg
	}

	roundTo { |operand|
		^this.round(operand)
	}

	rounded {
		^this.round(1)
	}

	runArrayFromRunsAndValues { |values|
		var runs = this;
		var size = runs.sum;
		var answer = Array.newClear(size);
		var index = 0;
		(0 .. runs.size - 1).do { |runIndex|
			runs[runIndex].timesRepeat {
				answer[index] = values[runIndex];
				index = index + 1
			}
		}
		^answer;
	}

	runArrayFromAssociations {
		var runs = this.collect { |each|
			each.key
		};
		var values = this.collect { |each|
			each.value
		};
		^runs.runArrayFromRunsAndValues(values)
	}

	runArray { |maybeValues|
		maybeValues.ifNil {
			^this.runArrayFromAssociations
		} {
			^this.runArrayFromRunsAndValues(maybeValues)
		}
	}

	second {
		^this[1]
	}

	shuffled {
		^this.scramble
	}

	third {
		^this[2]
	}

	transposed {
		/*
		0.to(this.first.size - 1).collect { |c|
			this.collect { |r|
				r[c]
			}
		}
		*/
		^this.flop
	}

	truncateTo { |operand|
		^this.trunc(operand)
	}

}

// Array math.  Note: add is not allowed since Sc arrays are not fixed size.

+ Symbol {

	isSymbol {
		^true
	}

	nameSendersOf {
		^this.sendersOf.collect { |aMethod|
			"%>>%".format(aMethod.ownerClass.name, aMethod.name)
		}
	}

	printSendersOf {
		this.nameSendersOf.do { |aSymbol|
			aSymbol.postln
		}
	}

	sendersOf {
		^Class.findAllReferences(this);
	}

	value { |...p|
		// Allow idioms such as c.sort('>') and c.sortOn('name', '<=').
		p.isEmpty.if {
			^this
		} {
			^p[0].performList(this, p.copyRange(1, p.size - 1))
		}
	}

}

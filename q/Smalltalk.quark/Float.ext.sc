+Float {

	degreesToRadians {
		// Answer the receiver in radians. Assumes the receiver is in degrees.
		var radiansPerDegree = pi / 180.0;
		^this * radiansPerDegree
	}

	ln {
		// Smalltalk x log => x log: 10 (prefer ln & log2 & log10) ; Haskell log x => x `logBase` e
		^this.log
	}

	radiansToDegrees {
		// Answer the receiver in degrees. Assumes the receiver is in radians.
		var radiansPerDegree = pi / 180.0;
		^this / radiansPerDegree
	}

	rounded {
		^this.round(1)
	}

	//----

	*e {
		^2.718281828459
	}

	*infinity {
		^inf
	}

	*radiansPerDegree {
		^ pi / 180.0
	}

}

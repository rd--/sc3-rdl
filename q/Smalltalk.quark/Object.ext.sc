+Object {

	also { |aFunction|
		aFunction.value(this);
		^this
	}

	ifNil { |nilBlock ifNotNil|
		(ifNotNil != nil).if {
			^ifNotNil.value(this)
		}
	}

	ifNotNil { |notNilBlock ifNil|
		^notNilBlock.value(this)
	}

	in { |aBlock|
		^aBlock.value(this)
	}

	isBoolean {
		^false
	}

	isComplex {
		^false
	}

	isDictionary {
		^false
	}

	isSymbol {
		^false
	}

	locateMethod { |name|
		var method = this.class.findMethod(name);
		method.notNil.if {
			^method
		} {
			var parent = this.superclass;
			parent.notNil.if {
				^parent.locateMethod(name)
			} {
				^nil
			}
		}
	}

	printString {
		^this.asString
	}

	yourself {
		^this
	}

}

+Complex {

	closeTo { |aComplex|
		^aComplex.isComplex && {
			this.real.closeTo(aComplex.real) && {
				this.imag.closeTo(aComplex.imag)
			}
		}
	}

	isComplex {
		^true
	}

}

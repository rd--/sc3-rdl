+Bag {

	sortedCounts {
		var answer = SortedList(8) { |x y|
			x >= y
		};
		this.contents.associationsDo { |anAssociation|
			answer.add(anAssociation.value -> anAssociation.key)
		};
		^answer.array
	}

	sortedElements {
		^this.contents.associations.sort
	}

}

+Number {

	centiSeconds {
		^Duration(this / 100)
	}

	degreesToRadians {
		^this.asFloat.degreesToRadians
	}

	microSeconds {
		^Duration(this / 1000000)
	}

	milliSeconds {
		^Duration(this / 1000)
	}

	radiansToDegrees {
		^this.asFloat.radiansToDegrees
	}

	seconds {
		^Duration(this)
	}

}

Generator {

	var routine, next;

	atEnd {
		^next == nil
	}

	contents {
		^this.contentsList.asArray
	}

	contentsList {
		var aList = List.new(8);
		this.do { |x|
			aList.add(x)
		};
		^aList
	}

	do { |aFunction|
		aFunction.value(next);
		routine.do(aFunction)
	}

	initializeOn { |aBlock|
		routine = Routine {
			aBlock.value(this);
			nil
		};
		next = routine.next(nil)
	}

	next {
		this.atEnd.ifFalse {
			var previous = next;
			next = routine.next(nil);
			^previous
		}
	}

	nextPut { |aValue|
		// stream procotol; synonym for yield
		aValue.yield
	}

	peek {
		^next
	}

	reset {
		routine.reset
	}

	value { |aValue|
		// value protocol; synonym for yield
		aValue.yield
	}

	yield { |aValue|
		aValue.yield
	}

	//--------

	*on { |aBlock|
		^super.new.initializeOn(aBlock)
	}

	*inject { |aValue aBlock|
		^this.on { |g|
			var last;
			last = aValue;
			inf.do {
				g.yield(last);
				last = aBlock.value(last)
			}
		}
	}

}

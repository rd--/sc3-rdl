+Function {

	repeat {
		// Evaluate the receiver repeatedly, ending only if the block explicitly returns.
		^{
			this.value;
			true
		}.whileTrue
	}

	whileTrue { |aBlock|
		^this.while(aBlock)
	}

	whileFalse { |aBlock|
		^{
			this.value.not
		}.while(aBlock)
	}

}

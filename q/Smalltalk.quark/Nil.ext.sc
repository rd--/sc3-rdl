+Nil {

	ifNil { |nilBlock ifNotNil|
		^nilBlock.value
	}

	ifNotNil { |notNilBlock ifNil|
		^(ifNil != nil).if {
			ifNil.value
		}
	}

}

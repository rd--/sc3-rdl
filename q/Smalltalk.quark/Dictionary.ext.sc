+Dictionary {

	associations {
		var answer = List();
		this.associationsDo { |each|
			answer.add(each)
		};
		^answer
	}

	isDictionary {
		^true
	}

}

+Collection {

	anyOne {
		this.do { |each|
			^ each
		};
		"Collection>>anyOne: empty collection".error
	}

	collectInto { |aBlock aCollection|
		^aCollection.fillFromWith(this, aBlock)
	}

	fillFromWith { |aCollection aBlock|
		aCollection.do { |each|
			this.add(aBlock.value(each))
		}
	}

	histogramOf { |aBlockOrNil|
		var answer = Bag();
		this.collectInto(aBlockOrNil ? { |each|
			each
		}, answer);
		^answer
	}

	ifEmpty { |emptyBlock nonEmptyBlockOrNil|
		^this.isEmpty.if(emptyBlock, nonEmptyBlockOrNil)
	}

	includesAllEqual { |aCollection|
		aCollection.do { |item|
			this.includesEqual(item).ifFalse {
				^false
			}
		};
		^true
	}

	isOfSameSizeCheck { |otherCollection|
		(otherCollection.size == this.size).ifFalse {
			"Collection>>isOfSameSizeCheck".error
		}
	}

	mostCommonItem {
		^this.asSet.collect { |i|
			this.occurrencesOf(i) -> i
		}.maxItem.value
	}

	reduce { |binaryBlock|
		// Apply the argument, binaryBlock cumulatively to the elements of the receiver.
		// For sequenceable collections the elements will be used in order, for unordered
		// collections the order is unspecified.
		var first, nextValue;
		first = true;
		this.do { |each|
			first.if {
				nextValue = each;
				first = false
			} {
				nextValue = binaryBlock.value(nextValue, each)
			}
		};
		first.if {
			this.error("EmptyCollection")
		};
		^nextValue
	}

	sorted { |sortBlockOrNil|
		^this.asArray.sort(sortBlockOrNil)
	}


}

+SimpleNumber {

	closeTo { |aNumber|
		aNumber.isNumber.if {
			^(this - aNumber).abs < 0.0001
		} {
			^false
		}
	}

	fractionPart {
		^this - this.roundTowardsZero
	}

	ln {
		^this.asFloat.ln
	}

	negated {
		^this.neg
	}

	roundDown { |aNumber = 1.0, adverb|
		^this.neg.roundUp(aNumber, adverb).neg
	}

	rounded {
		^this.round
	}

	roundTowardsZero { |aNumber = 1.0, adverb|
		(this < 0).if {
			^this.roundUp(aNumber, adverb)
		} {
			^this.roundDown(aNumber, adverb)
		}
	}

	to { |end, step = 1|
		^this.series(this + step, end)
	}

	truncated {
		^this.error('see roundDown')
	}

}

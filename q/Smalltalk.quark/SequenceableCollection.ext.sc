+SequenceableCollection {

	allButFirst {
		^this.copyRange(1, this.size - 1)
	}

	closeTo { |aSequence|
		^this.hasEqualElementsBy(aSequence) { |lhs rhs|
			lhs.closeTo(rhs)
		}
	}

	concatenation {
		^this.flatten
	}


	fisherYatesShuffle {
		// Durstenfeld's version of the Fisher-Yates shuffle
		(this.size - 1).toByDo(1, -1) { |index|
			this.swapWith(index, index.rand)
		}
	}

	hasEqualElementsBy { |aSequence aBlock|
		^(this.size == aSequence.size).and {
			this.indicesDo { |index|
				aBlock.value(this[index], aSequence[index]).ifFalse {
					^false
				}
			};
			true
		}
	}

	hasEqualElements { |aSequence|
		^this.hasEqualElementsBy(aSequence) { |lhs rhs|
			lhs == rhs
		}
	}

	indices {
		^(0 .. this.size - 1)
	}

	indicesDo { |aBlock|
		this.size.do(aBlock)
	}

	nth { |index|
		^this[index - 1]
	}

	reverseDo { |aBlock|
		/*
		Evaluate aBlock with each of the receiver's elements as the argument,
		starting with the last element and taking each in sequence up to the
		first. For SequenceableCollections, this is the reverse of the enumeration
		for do:.
		*/
		(this.size - 1).toByDo(0, -1) { |index|
			aBlock.value(this[index])
		}
	}

	reverseInPlace {
		// Reverse this collection in place
		var start = 0;
		var end = this.size - 1;
		{ start < end }.while {
			var temp = this[start];
			this[start] = this[end];
			this[end] = temp;
			start = start + 1;
			end = end - 1
		}
	}

	reversed {
		// Answer a copy of the receiver with element order reversed.
		var n = this.size;
		var result = this.species.newClear(n);
		var src = n;
		0.toDo(n - 1) { |dst|
			src = src - 1;
			result[dst] = this[src]
		};
		^ result
	}

	shuffled {
		^this.fisherYatesShuffle
	}

	sorted {
		^this.copy.sort(nil)
	}

	sortWith { |keyFunc compareFunc|
		// [t] -> (t -> u) -> (u -> u -> Bool) -> [t]
		compareFunc.ifNil {
			compareFunc = { |i j|
				i <= j
			}
		};
		^this.copy.sort { |p q|
			compareFunc.value(keyFunc.value(p),keyFunc.value(q))
		}
	}

	swapWith { |oneIndex anotherIndex|
		// Move the element at oneIndex to anotherIndex, and vice-versa.
		var element = this[oneIndex];
		this[oneIndex] = this[anotherIndex];
		this[anotherIndex] = element
	}

	withCollect { |otherCollection twoArgBlock|
		// Collect and return the result of evaluating twoArgBlock with corresponding elements from this collection and otherCollection.
		var result;
		this.isOfSameSizeCheck(otherCollection);
		result = this.species.newClear(this.size);
		0.to(this.size - 1).do { |index|
			result[index] = twoArgBlock.value(this[index], otherCollection[index])
		};
		^ result
	}

	withIndexCollect { |twoArgBlock|
		^ this.collect(twoArgBlock)
	}

}

+ Integer {

	benchFib {
		// Handy send-heavy benchmark
		// (result // seconds to run) = approx calls per second
		// 138,000 on a Mac 8100/100
		^(this < 2).if {
			1
		} {
			(this - 1).benchFib + (this - 2).benchFib + 1
		}
	}

	benchmark  {
		// Handy bytecode-heavy benchmark
		// (500000 // time to run) = approx bytecodes per second
		// 5000000 // (Time millisecondsToRun: [10 benchmark]) * 1000
		// 3,059,000 on a Mac 8100/100
		var size = 8190;
		var count;
		1.to(this).do {
			var flags = Array.fill(size, true);
			count = 0;
			1.to(size).do { |i|
				flags[i - 1].if {
					var prime = i + 1;
					var k = i + prime;
					{ k <= size}.whileTrue {
						flags[k - 1] = false;
						k = k + prime
					};
					count = count + 1
				}
			}
		};
		^count
	}

	timesRepeat { |aBlock|
		^this.do(aBlock)
	}

	toByDo { |to by aBlock|
		^Interval(this, to, by).do(aBlock)
	}

	toDo { |to aBlock|
		^Interval(this, to, 1).do(aBlock)
	}

}

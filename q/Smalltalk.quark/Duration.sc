Duration : Magnitude {

	var <>seconds;

	== { |other|
		^seconds == other.seconds
	}

	init { |aNumber|
		seconds = aNumber
	}

	wait {
		seconds.wait
	}

	// ----

	*new { |seconds|
		^super.new.init(seconds)
	}

}

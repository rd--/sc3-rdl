+ Interval {

	* { |number|
		^(number == 0).if {
			this.asArray * 0
		} {
			Interval(start * number, end * number, step * number)
		}
	}

	+ { |number|
		^Interval(start + number, end + number, step)
	}

	- { |number|
		^Interval(start - number, end - number, step)
	}

	/ { |number|
		^(number == 0).if {
			this.asArray / 0
		} {
			Interval(start / number, end / number, step / number)
		}
	}

	atRandom {
		^this.asArray.choose
	}

}

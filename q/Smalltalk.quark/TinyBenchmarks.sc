// Report the results of running the two tiny Squeak benchmarks.
//  ar 9/10/1999: Adjusted to run at least 1 sec to get more stable results
// On a 292 MHz G3 Mac: 23,000,000 bytecodes/sec; 980,000 sends/sec
// On a 400 MHz PII/Win98:  18,000,000 bytecodes/sec; 1,100,000 sends/sec
// On a 2800 MHz i7:  1,200,000,000 bytecodes/sec; 25,000,000 sends/sec
// On a 2800 MHz i7 (CogVM):  1,700,000,000 bytecodes/sec; 260,000,000 sends/sec
// On a 1700 MHz i5:
//    sclang: 75,000,000 bytecodes/sec, 12,000,000 sends/sec
//    squeak: 2,400,000,000 bytecodes/sec; 230,000,000 sends/sec
//    (2400 / 75) = 30 ; (230 / 12) = 20
//    spl/deno: 116,363,636 bytecodes/sec; 5,035,110 sends/sec
TinyBenchmarks {

	run {
		var n1, n2, r, t1, t2;
		n1 = 1;
		{
			t1 = { n1.benchmark }.bench(false);
			t1 < 1
		}.while {
			n1 = n1 * 2
		};
		// Note: #benchmark's runtime is about O(n)
		n2 = 28;
		{
			t2 = { r = n2.benchFib }.bench(false);
			t2 < 1
		}.while {
			n2 = n2 + 1
		};
		// Note: #benchFib's runtime is about O(k^n), where k is the golden number = (1 + 5 sqrt) / 2 = 1.618....
		^ "% bytecodes/sec, % sends/sec".format(((n1 * 500000) / t1).round, (r / t2).round)
	}

	//----

	*run {
		^super.new.run
	}

}


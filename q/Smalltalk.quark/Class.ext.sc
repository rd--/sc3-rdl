+Class {

	allSubclassesOf {
		var answer = this.subclasses;
		(answer.size == 0).if {
			^[]
		} {
			^answer ++ answer.collect(_.allSubclassesOf).flatten
		}
	}

	subclassTreeOf {
		var subclasses = this.subclasses;
		subclasses.isNil.if {
			^this
		} {
			^this -> subclasses.copy.sortWith('name', '<=').collect { |class|
				class.subclassTreeOf
			}
		}
	}

	superclassesOf {
		var aClass = this;
		var answer = List.new(8);
		answer.add(aClass);
		{
			aClass != Object
		}.whileTrue {
			aClass = aClass.superclass;
			answer.add(aClass)
		};
		^answer
	}

}

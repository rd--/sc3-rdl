+Array {

	dividedBy { |operand|
		^this / operand
	}

	midiCps {
		^this.midicps
	}

	midiRatio {
		^this.midiratio
	}

	minus { |operand|
		^this - operand
	}

	mix {
		^this.sum
	}

	mulAdd { |mul add|
		^this * mul + add
	}

	times { |operand|
		^this * operand
	}

}

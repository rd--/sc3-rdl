+Integer {

	upTo { |to|
		^Interval(this, to, 1)
	}

}

+SimpleNumber {

	cpsMidi {
		^this.cpsmidi
	}

	linRand0 {
		^this.linrand
	}

	midiCps {
		^this.midicps
	}

	midiRatio {
		^this.midiratio
	}

	rand0 {
		^this.rand
	}

	ratioMidi {
		^this.ratiomidi
	}

}

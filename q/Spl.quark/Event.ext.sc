+Event {

	groupControls {
		var defaults = List(), names = List(), shape = List(), control, index = 0, answer = ();
		this.keysValuesDo { |name defaultValue|
			shape.add(defaultValue);
			defaultValue.isArray.if {
				var qualifier = 1;
				defaultValue.collect { |each|
					var qualifiedName = name ++ qualifier.asString;
					qualifier = qualifier + 1;
					names.add(qualifiedName);
					defaults.add(each)
				}
			} {
				names.add(name);
				defaults.add(defaultValue)
			}
		};
		control = Control.names(names).kr(defaults).asArray.reshapeLike(shape);
		this.keysValuesDo { |name defaultValue|
			answer.add(name -> control[index]);
			index = index + 1
		};
		^answer
	}

	localControls {
		^this.collect { |defaultValue name|
			defaultValue.isArray.if {
				var qualifier = 1;
				defaultValue.collect { |each|
					var qualifiedName = name ++ qualifier.asString;
					qualifier = qualifier + 1;
					NamedControl.kr(qualifiedName, each)
				}
			} {
				NamedControl.kr(name, defaultValue)
			}
		}
	}

}

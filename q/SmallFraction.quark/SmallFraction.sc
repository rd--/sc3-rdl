SmallFraction : Number {

	var <numerator, <denominator;

	*newNotReduced { |numerator denominator|
		^super.newCopyArgs(numerator, denominator)
	}

	*new { |numerator denominator|
		^super.newCopyArgs(numerator, denominator).reduced
	}

	* { |aNumber|
		aNumber.isFraction.if {
			var d1 = numerator.gcd(aNumber.denominator);
			var d2 = denominator.gcd(aNumber.numerator);
			((d2 == denominator).and {
				d1 == aNumber.denominator
			}).if {
				^(numerator.div(d1)) * (aNumber.numerator.div(d2))
			} {
				^SmallFraction(
					(numerator.div(d1)) * (aNumber.numerator.div(d2)),
					(denominator.div(d2)) * (aNumber.denominator.div(d1))
				).normalized
			}
		} {
			^aNumber.performBinaryOpOnSmallFraction('*', this)
		}
	}

	+ { |aNumber|
		aNumber.isInteger.if {
			^SmallFraction(numerator + (denominator * aNumber), denominator)
		} {
			aNumber.isFraction.if {
				var d = denominator.gcd(aNumber.denominator);
				var d1 = aNumber.denominator.div(d);
				var d2 = denominator.div(d);
				var n = (numerator * d1) + (aNumber.numerator * d2);
				d1 = d1 * d2;
				d2 = n.gcd(d);
				n = n.div(d2);
				d = d1 * d.div(d2);
				(d == 1).if {
					^n
				} {
					^SmallFraction(n, d)
				}
			} {
				^aNumber.performBinaryOpOnSmallFraction('+', this)
			}
		}
	}

	- { |aNumber|
		aNumber.isInteger.if {
			^SmallFraction(numerator - (denominator * aNumber), denominator)
		} {
			aNumber.isFraction.if {
				^this + aNumber.negated
			} {
				^aNumber.performBinaryOpOnSmallFraction('-', this)
			}
		}
	}

	/ { |aNumber|
		aNumber.isFraction.if {
			aNumber.isSmallFraction.if {
				^this * aNumber.reciprocal
			} {
				^this * SmallFraction(1, aNumber)
			}
		} {
			^aNumber.performBinaryOpOnSmallFraction('/', this)
		}
	}

	** { |anInteger|
		anInteger.isInteger.if {
			^this.raisedToInteger(anInteger)
		} {
			"SmallFraction>>** not an integer".error
		}
	}

	== { |aFraction|
		aFraction.isFraction.and {
			(numerator == aFraction.numerator).and {
				^denominator == aFraction.denominator
			}
		};
		^false
	}

	asFloat {
		^numerator / denominator
	}

	exactSmallFraction {
		^this
	}

	gcd { |aSmallFraction|
		var d = denominator.gcd(aSmallFraction.denominator);
		^SmallFraction(
			(numerator * (aSmallFraction.denominator.div(d))).gcd(aSmallFraction.numerator * (denominator.div(d))),
			(denominator.div(d) * aSmallFraction.denominator)
		)
	}

	isFraction {
		^true
	}

	isSmallFraction {
		^true
	}

	lcm { |n|
		^this.div(this.gcd(n)) * n
	}

	negated {
		^SmallFraction(numerator.neg, denominator)
	}

	negative {
		^numerator.negative
	}

	normalize {
		^this.copy.normalized
	}

	normalized {
		(denominator == 0).if {
			"SmallFraction>>normalized: zeroDenominatorError".error
		} {
			var x = numerator * denominator.sign;
			var y = denominator.abs;
			var d = x.gcd(y);
			numerator = x.div(d);
			denominator = y.div(d);
		}
	}

	performBinaryOpOnSimpleNumber { |aSelector aNumber adverb|
		^aNumber.exactSmallFraction.perform(aSelector, this, adverb)
	}

	printOn { |stream|
		stream <<  this.printString
	}

	printString {
		^numerator.printString ++ "/" ++ denominator.printString
	}

	raisedToInteger { |anInteger|
		(anInteger == 0).if {
			^1
		} {
			(anInteger < 0).if {
				^this.reciprocal.raisedToInteger(anInteger.neg)
			} {
				^SmallFraction(
					numerator.raisedToInteger(anInteger),
					denominator.raisedToInteger(anInteger)
				)
			}
		}
	}

	reciprocal {
		(numerator.abs == 1).if {
			^denominator * numerator
		} {
			^SmallFraction(denominator, numerator).normalized
		}
	}

	reduce {
		^this.copy.reduced
	}

	reduced {
		(denominator == 0).if {
			'SmallFraction>>reduced: zeroDenominatorError'.error
		} {
			var x = numerator * denominator.sign;
			var y = denominator.abs;
			var d = x.gcd(y);
			numerator = x.div(d);
			denominator = y.div(d);
			(denominator == 1).if {
				^numerator
			} {
				^this
			}
		}
	}

	truncated {
		^numerator.quotient(denominator)
	}

}

+ Integer {

	%% { |anInteger|
		^SmallFraction(this, anInteger)
	}

	exactSmallFraction {
		^SmallFraction.newNotReduced(this, 1)
	}

	denominator {
		^1
	}

	isFraction {
		^true
	}

	numerator {
		^this
	}

	performBinaryOpOnSmallFraction { |aSelector aFraction|
		^aFraction.perform(aSelector, SmallFraction(this, 1))
	}

	printString {
		^this.asString
	}

	raisedToInteger { |anInteger|
		^(this ** anInteger).asInteger
	}

	smallFraction { |anInteger|
		^SmallFraction(this, anInteger)
	}

}

+ String {

	parseInteger {
		^this.asInteger
	}

	parseSmallFraction {
		var stringArray = this.split($/);
		^SmallFraction(
			stringArray[0].parseInteger,
			stringArray[1].parseInteger
		)
	}

}

+ Array {

	exactSmallFraction {
		(this.size == 2).if {
			^SmallFraction(this[0], this[1])
		}
	}

}

+ Float {

	approximateSmallFraction { |denominatorLimit=100|
		^this.asFraction(denominatorLimit, false).exactSmallFraction
	}

	exactSmallFraction {
		var approximate = this.approximateSmallFraction;
		(approximate.asFloat == this).if {
			^approximate
		} {
			"Float>>exactSmallFraction not exact fraction".error
		}
	}

	performBinaryOpOnSmallFraction { |aSelector aFraction|
		^aFraction.perform(aSelector, this.exactSmallFraction)
	}

}

+ Number {

	f {
		^this.exactSmallFraction
	}

}

+ Object {

	isFraction {
		^false
	}

	isSmallFraction {
		^false
	}

}

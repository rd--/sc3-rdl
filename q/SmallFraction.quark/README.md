# SmallFraction

Implement SmallFraction class representing rational numbers.

SuperCollider does not implement arbitrary precision integers.

See [SmallFraction.help.scd](https://gitlab.com/rd--/sc3-rdl/-/blob/master/q/SmallFraction.quark/SmallFraction.help.scd)

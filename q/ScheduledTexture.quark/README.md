# ScheduledTexture

Implements _overlap_, _xfade_ and _spawn_ texture generators similar to those in Sc2.

See [ScheduledTexture.help.scd](https://gitlab.com/rd--/sc3-rdl/-/blob/master/q/ScheduledTexture.quark/ScheduledTexture.help.scd)

See also: _GraphTexture.quark_

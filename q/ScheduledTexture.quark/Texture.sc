Texture {

	//--------

	*addEnvelope { |newEventFunc counter|
		^{
			var sustainTime = NamedControl.kr(name: 'sustainTime', values: 4);
			var transitionTime = NamedControl.kr(name: 'transitionTime', values: 1);
			var env = Env.new(levels: [0,1,1,0], times: [transitionTime,sustainTime,transitionTime], curve: 'sin');
			var amp = EnvGen.kr(envelope: env, gate: 1, levelScale: 1, levelBias: 0, timeScale: 1, doneAction: 2);
			var snd = newEventFunc.value(counter);
			Out.ar(bus: 0, channelsArray: snd * amp)
		}
	}

	*overlapRoutine { |newEventFunc, sustainTime = 4.0, transitionTime = 4.0, overlap = 2, numChannels = 2, maxRepeats = inf, postProcess = nil, bus = 0|
		var period = (sustainTime + (transitionTime * 2)) / overlap;
		var routine = {
			maxRepeats.do { |i|
				Texture.addEnvelope(newEventFunc: newEventFunc, counter: i).play(
					args: [
						'sustainTime': sustainTime,
						'transitionTime': transitionTime
					]
				);
				period.wait
			}
		}.fork;
		postProcess.notNil.if {
			Texture.postProcess(treatment: postProcess, bus: bus, numChannels: numChannels)
		};
		^routine
	}

	*postProcess { |treatment, bus = 0, numChannels = 2|
		{
			var z = In.ar(bus: bus, numChannels: numChannels);
			z = treatment.value(z);
			ReplaceOut.ar(bus: bus, channelsArray: z);
		}.play(addAction: 'addToTail');
	}

	*spawnRoutine { |newEventFunc, numChannels = 2, nextTime, maxRepeats = inf, postProcess = nil, bus = 0|
		var routine = {
			maxRepeats.do { |i|
				{
					Out.ar(bus: 0, channelsArray: newEventFunc.value(i))
				}.play;
				nextTime.value.wait
			}
		}.fork;
		postProcess.notNil.if {
			Texture.postProcess(treatment: postProcess, bus: bus, numChannels: numChannels)
		};
		^routine
	}

	*xfadeRoutine { |newEventFunc, sustainTime = 4.0, transitionTime = 4.0, numChannels = 2, maxRepeats = inf, postProcess = nil, bus = 0|
		var period = sustainTime + transitionTime;
		var routine = {
			maxRepeats.do { |i|
				Texture.addEnvelope(newEventFunc: newEventFunc, counter: i).play(
					args: [
						'sustainTime': sustainTime,
						'transitionTime': transitionTime
					]
				);
				period.wait
			}
		}.fork;
		postProcess.notNil.if {
			Texture.postProcess(treatment: postProcess, bus: bus, numChannels: numChannels)
		};
		^routine
	}

}

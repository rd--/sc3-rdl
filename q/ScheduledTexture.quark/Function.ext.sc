+Function {

	overlap { |sustainTime = 4.0, transitionTime = 4.0, overlap = 2|
		^Texture.overlapRoutine(this, sustainTime, transitionTime, overlap)
	}

	spawn { |nextTime = 4|
		^Texture.spawnRoutine(this, 2, nextTime)
	}

	xfade { |sustainTime = 4.0, transitionTime = 4.0|
		^Texture.xfadeRoutine(this, sustainTime, transitionTime)
	}

}

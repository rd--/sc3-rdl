SfAcquire {

	//----

	classvar nextBuffer;

	*initClass {
		nextBuffer = 100
	}

	*new { |urlOrKey numberOfChannels channelIndices|
		var url = SfDict.at(urlOrKey);
		var cacheValue = SfCache.at(url);
		cacheValue.isNil.if {
			var sf = SoundFile.openRead(url.standardizePath);
			(numberOfChannels != sf.numChannels).if {
				"SfAcquire: channel mismatch".die
			} {
				var bufferArray = 0.to(sf.numChannels - 1).collect { |channelIndex|
					var buffer = Buffer.new(Server.default, sf.numFrames, 1, nextBuffer + channelIndex);
					buffer.allocReadChannel(url, 0, sf.numFrames, channelIndex, nil);
				};
				SfCache.put(url, bufferArray);
				nextBuffer = nextBuffer + sf.numChannels;
				sf.close;
				cacheValue = bufferArray
			}
		};
		channelIndices.isArray.if {
			^channelIndices.collect { |item|
				cacheValue.wrapAt(item - 1)
			}
		} {
			^cacheValue.wrapAt(channelIndices - 1)
		}
	}

}

/*
SfAcquire("crotale-d6", 1, [1])
SfAcquire("harp-a4", 2, [1, 2])
SfAcquire("piano-c5", 2, [1, 2])
*/

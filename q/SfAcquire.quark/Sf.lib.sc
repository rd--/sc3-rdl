SfFrames {

	*kr { |sfBufferArray|
		^BufFrames.kr(sfBufferArray.asArray.first)
	}

	*new { |sfBufferArray|
		^SfFrames.kr(sfBufferArray)
	}

}

SfDur {

	*kr { |sfBufferArray|
		^BufDur.kr(sfBufferArray.asArray.first)
	}

	*new { |sfBufferArray|
		^SfDur.kr(sfBufferArray)
	}

}

SfSampleRate {

	*kr { |sfBufferArray|
		^BufSampleRate.kr(sfBufferArray.asArray.first)
	}

	*new { |sfBufferArray|
		^SfSampleRate.kr(sfBufferArray)
	}

}

SfRateScale {

	*kr { |sfBufferArray|
		^BufRateScale.kr(sfBufferArray.asArray.first)
	}

	*new { |sfBufferArray|
		^SfRateScale.kr(sfBufferArray)
	}

}

SfRead {

	*ar { |sfBufferArray, phase = 0, loop = 1, interpolation = 2|
		^BufRd.ar(1, sfBufferArray, phase, loop, interpolation)
	}

	*new { |sfBufferArray, phase = 0, loop = 1, interpolation = 2|
		^SfRead.ar(sfBufferArray, phase, loop, interpolation)
	}

}

SfPlay {

	*ar { |sfBufferArray, rate = 1, trigger = 1, startPos = 0, loop = 0|
		^PlayBuf.ar(1, sfBufferArray, rate, trigger, startPos, loop, 0)
	}

	*new { |sfBufferArray, rate = 1, trigger = 1, startPos = 0, loop = 0|
		^SfPlay.ar(sfBufferArray, rate, trigger, startPos, loop)
	}

}

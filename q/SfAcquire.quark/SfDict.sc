SfDict {

	//----

	classvar sfDict;

	*at { |key|
		sfDict.includesKey(key).if {
			^sfDict[key]
		} {
			^key
		}
	}

	*initClass {
		sfDict = Dictionary.newFrom([
			"CrotaleD6", "/home/rohan/sw/jssc3/flac/CrotaleD6.flac",
			"HarpA4", "/home/rohan/sw/jssc3/flac/HarpA4.flac",
			"PianoC5", "/home/rohan/sw/jssc3/flac/PianoC5.flac",
			"Floating", "/home/rohan/sw/jssc3/flac/Floating.flac",
			"Then", "/home/rohan/sw/jssc3/flac/Then.flac"
		])
	}

	*put { |key value|
		sfDict[key] = value;
		^nil
	}

}

/*
SfDict.at("CrotaleD6")
SfDict.at("HarpA4")
SfDict.at("PianoC5")
*/

// The cache should be per-Server and invalidated whenever scsynth is started or stopped.
// For now add ServerBoot.add({ SfCache.invalidate }) to startup.scd

SfCache {

	//----

	classvar <sfCache;

	*at { |key|
		^sfCache[key]
	}

	*initClass {
		sfCache = Dictionary.new
	}

	*invalidate {
		this.initClass
	}

	*put { |key value|
		sfCache[key] = value;
		^nil
	}

}

/*
SfCache.sfCache
*/

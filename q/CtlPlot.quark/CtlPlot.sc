CtlPlot {

	var <>bus; // Bus
	var <>delayTime; // Float -- delay time between reading control data
	var <>drawFunc; // Function
	var <>drawRate; // Float -- draw rate as multiplier for data read rate
	var <>drawRoutine; // Routine
	var <>getFunc; // Function
	var <>getRoutine; // Routine
	var <>indexZero; // Int -- index of first control bus
	var <>numChannels; // Int -- number of channels of control data
	var <>numFrames; // Int -- number of frames of control data at plot view
	var <>plot; // Plot
	var <>plotFrames; // Int - number of frames of data to plot
	var <>server; // Server
	var <>values; // Array[[Float]]

	clear {
		values.fill(0 ! numChannels);
		this.draw
	}

	close {
		this.stop;
		plot.parent.close
	}

	draw {
		plot.setValue(arrays: values.flop, findSpecs: true, refresh: true, separately: false)
	}

	init { |numFrames numChannels indexZero plotFrames delayTime drawRate|
		this.numFrames = numFrames;
		this.numChannels = numChannels;
		this.indexZero = indexZero;
		this.plotFrames = plotFrames;
		this.delayTime = delayTime;
		this.drawRate = drawRate;
		values = Array.fill(numFrames) {
			0 ! numChannels
		};
		bus = Bus.new(rate: 'control', index: indexZero, numChannels: numChannels, server: server);
		plot = values.plot;
		plot.parent.onClose {
			this.stop
		};
		this.start
	}

	start {
		getFunc = {
			plotFrames.do { |i|
				bus.get { |x|
					values.wrapPut(i, x)
				};
				delayTime.wait
			}
		};
		drawFunc = {
			(plotFrames / drawRate).do {
				this.draw;
				(delayTime * drawRate).wait
			}
		};
		getRoutine = getFunc.fork(clock: TempoClock);
		drawRoutine = drawFunc.fork(clock: AppClock)
	}

	stop {
		getRoutine.stop;
		drawRoutine.stop
	}

	//--------

	*new { |numFrames numChannels indexZero plotFrames delayTime drawRate|
		^super.new.init(numFrames, numChannels, indexZero, plotFrames, delayTime, drawRate)
	}

}

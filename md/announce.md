Polyglot SuperCollider

Polyglot SuperCollider is an experiment in collating SuperCollider work from multiple languages.

PolyglotGraphBrowser is a simple interface for browsing the resulting archive.

It currently knows about graphs written in:

- .fs = Forth
- .hs = Haskell
- .scala = Scala
- .scd = SuperCollider
- .sch = Scheme (Haskell Syntax)
- .scm = Scheme
- .st = Smalltalk

Polyglot SuperCollider is research quality, it has minimal documentation and though it's written in SuperCollider and doesn't need other language interpreters some functions require external processes that aren't included here.

PolyglotGraphBrowser is at <http://rd.slavepianos.org/t/sc3-rdl>

There is an archive of graphs at <http://rd.slavepianos.org/t/hsc3-graphs> (in the "db" directory) and a program for autogenerating the archive.

There are some demonstration videos at <http://rd.slavepianos.org/?t=sc3-rdl&e=md/video.md>

Best,
Rohan

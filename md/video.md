# Polyglot SuperCollider &etc.

Graph Voicing - Parameters & UI - 2021-11-08

<iframe src="https://player.vimeo.com/video/643332351" frameborder="0" allowfullscreen="true" width="640" height="360" >
</iframe>

Polyglot SuperCollider - Scala, Texture - 2021-06-14

<iframe src="https://player.vimeo.com/video/562653821" frameborder="0" allowfullscreen="true" width="640" height="360" >
</iframe>

Polyglot SuperCollider - pmOsc - 2021-05-13

<iframe src="https://player.vimeo.com/video/548716508" frameborder="0" allowfullscreen="true" width="640" height="360" >
</iframe>

Polyglot SuperCollider - f0.st - 2021-05-13

<iframe src="https://player.vimeo.com/video/548716543" frameborder="0" allowfullscreen="true" width="640" height="360" >
</iframe>

Polyglot SuperCollider - Smalltalk & Haskell - 2021-05-12

<iframe src="https://player.vimeo.com/video/548289719" frameborder="0" allowfullscreen="true" width="640" height="360" >
</iframe>

Polyglot SuperCollider - Smalltalk, Event Control, UI - 2021-05-06

<iframe src="https://player.vimeo.com/video/545822294" frameborder="0" allowfullscreen="true" width="640" height="360" >
</iframe>

SuperCollider - RCtlPlot - 2021-04-02

<iframe src="https://player.vimeo.com/video/540500850" frameborder="0" allowfullscreen="true" width="640" height="360" >
</iframe>

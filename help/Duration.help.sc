// Durations can be waited on
{
	var x = { SinOsc.ar(440, 0) * 0.1 }.play;
	150.centiSeconds.wait;
	x.release(1.5)
}.fork

// Duration ; keywords
{
	var x = {
		SinOsc.ar(
			freq: 440,
			phase: 0
		) * 0.1
	}.play;
	150.centiSeconds.wait;
	x.release(releaseTime: 1.5)
}.fork

//---- Duration
Duration(3) == 3.seconds
300.centiSeconds == 3000.milliSeconds
3000.milliSeconds == 3000000.microSeconds
